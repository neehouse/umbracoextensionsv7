﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Membership;
using Umbraco.Web;
using BCT.Media.Extensions;
using Umbraco.Web.Models.ContentEditing;
using BCT.Media.Models;
using Umbraco.Web.Models.Trees;

namespace BCT.Media.Delegate
{
    public class WebApiHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;


            switch (request.RequestUri.AbsolutePath.ToLower()) {
                case "/umbraco/backoffice/umbracoapi/media/getchildren":
                    return FilterMediaGetChildren(request, cancellationToken, user);
                case "/umbraco/backoffice/umbracoapi/media/getbyid":
                    return FilterFileBrowser(request, cancellationToken, user);
                default:
                    return base.SendAsync(request, cancellationToken);
            }
        }

        private Task<HttpResponseMessage> FilterMediaGetChildren(HttpRequestMessage request, CancellationToken cancellationToken, IUser user)
        {
            return base.SendAsync(request, cancellationToken)
                .ContinueWith(task =>
                {
                    var response = task.Result;

                    var items = ((Umbraco.Core.Models.PagedResult<Umbraco.Web.Models.ContentEditing.ContentItemBasic<Umbraco.Web.Models.ContentEditing.ContentPropertyBasic, Umbraco.Core.Models.IMedia>>)(((System.Net.Http.ObjectContent)(response.Content)).Value)).Items;

                    if (items != null)
                    {
                        items = items.Where(item => UserCanView(user, item));
                        ((Umbraco.Core.Models.PagedResult<Umbraco.Web.Models.ContentEditing.ContentItemBasic<Umbraco.Web.Models.ContentEditing.ContentPropertyBasic, Umbraco.Core.Models.IMedia>>)(((System.Net.Http.ObjectContent)(response.Content)).Value)).Items = items;
                    }
                    return response;
                });
        }

        private Task<HttpResponseMessage> FilterFileBrowser(HttpRequestMessage request, CancellationToken cancellationToken, IUser user)
        {
            var ms = ApplicationContext.Current.Services.MediaService;
            var media = ms.GetById(int.Parse(request.RequestUri.ParseQueryString().Get("id")));
            var user2NodePermissions = ms.GetUser2NodePermissions(media.Path, user);
            if (user2NodePermissions == null)
                user2NodePermissions = user.GetDefaultPermission(media.Id);

            return base.SendAsync(request, cancellationToken)
                .ContinueWith(task =>
                {
                    var response = task.Result;
                    var content = (Umbraco.Web.Models.ContentEditing.ContentItemDisplayBase<Umbraco.Web.Models.ContentEditing.ContentPropertyDisplay, Umbraco.Core.Models.IMedia>)((((System.Net.Http.ObjectContent)(response.Content)).Value));
                    if (content.ContentTypeName == "Folder")
                    {
                        if (!user2NodePermissions.AssignedPermissions.Contains("C"))
                        {
                            // find the folder browser property and replace the view.
                            var folderBrowserProperty = content.Properties.Where(x => x.Alias == "contents").FirstOrDefault();
                            if (folderBrowserProperty != null)
                            {
                                folderBrowserProperty.View = "/App_Plugins/BCT.Media/BasicFolderBrowser/folderbrowser.html";
                            }

                            var folderBrowser = content.Tabs.First().Properties.Where(x => x.Alias == "contents").FirstOrDefault();
                            if (folderBrowser != null)
                            {
                                folderBrowser.View = "/App_Plugins/BCT.Media/BasicFolderBrowser/folderbrowser.html";
                            }
                        }
                        ((System.Net.Http.ObjectContent)(response.Content)).Value = content;
                    }

                    return response;
                });
        }

        private bool UserCanView(IUser user, Umbraco.Web.Models.ContentEditing.ContentItemBasic<Umbraco.Web.Models.ContentEditing.ContentPropertyBasic, Umbraco.Core.Models.IMedia> item)
        {
            var ms = ApplicationContext.Current.Services.MediaService;
            
            var hasPermission = false;
            // check for set permissions
            var user2NodePermissions = ms.GetUser2NodePermissions(item.Path, user);
            if (user2NodePermissions == null)
                user2NodePermissions = user.GetDefaultPermission(int.Parse(item.Id.ToString()));

            if(user2NodePermissions!= null)
            {
                if (user2NodePermissions.AssignedPermissions.Contains("F"))
                {
                    hasPermission = true;
                }
            }
            else
            {
                //default permissions.
                hasPermission = user.DefaultPermissions.Contains("F");
            }
            return hasPermission;
        }
    }
}