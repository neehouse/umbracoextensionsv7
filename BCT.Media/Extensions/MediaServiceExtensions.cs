﻿using BCT.Media.Models.Rdbms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Membership;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.Repositories;
using Umbraco.Core.Persistence.UnitOfWork;
using Umbraco.Core.Services;
using Umbraco.Web.Models.ContentEditing;

namespace BCT.Media.Extensions
{
    public static class MediaServiceExtensions
    {
        public static void AssignMediaPermissions(this IMediaService mediaService, IMedia entity, char permission, int userId)
        {
            IDatabaseUnitOfWorkProvider _uowProvider = new PetaPocoUnitOfWorkProvider();
            RepositoryFactory _repositoryFactory = new RepositoryFactory();

            var uow = _uowProvider.GetUnitOfWork();
            using (var repository = _repositoryFactory.CreateMediaRepository(uow))
            {
                repository.AssignEntityPermission(entity, permission, userId);
            }
        }

        public static void DeleteMediaPermissions(this IMediaService mediaService, IMedia entity)
        {
            IDatabaseUnitOfWorkProvider _uowProvider = new PetaPocoUnitOfWorkProvider();
            RepositoryFactory _repositoryFactory = new RepositoryFactory();

            var uow = _uowProvider.GetUnitOfWork();
            using (var repository = _repositoryFactory.CreateMediaRepository(uow))
            {
                repository.DeleteEntityPermission(entity);
            }
        }

        public static IEnumerable<EntityPermission> GetNodePermissions(this IMediaService mediaService, IMedia entity)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var sql = new Sql();

            sql.Select("*")
               .From<User2NodePermissionDto>()
               .Where<User2NodePermissionDto>(dto => dto.NodeId == entity.Id)
               .OrderByDescending<User2NodePermissionDto>(dto => dto.NodeId);

            var result = db.Fetch<User2NodePermissionDto>(sql).ToArray();
            return ConvertToPermissionList(result);
        }

        public static EntityPermission GetUser2NodePermissions(this IMediaService mediaService, string path, IUser user)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var sql = @"SELECT Top 1 p.userId, p.nodeId, p.permission 
                        FROM umbracoUser2NodePermission p 
                            INNER JOIN umbracoNode n ON p.nodeId = n.id 
                        WHERE n.id IN (@path) AND p.userId = @userId
                        ORDER BY n.level DESC
                      ";

            //sql.Select("TOP 1 *")
            //   .From<User2NodePermissionDto>()
            //   .InnerJoin<NodeDto>().On<User2NodePermissionDto, NodeDto>(p => p.NodeId, )
            //   .Where<User2NodePermissionDto>(dto => entity.Path.Split(',').Contains(dto.NodeId.ToString()) && dto.UserId == user.Id)
            //   .OrderByDescending<User2NodePermissionDto>(dto => dto.NodeId);

            var result = db.Fetch<User2NodePermissionDto>(sql, new { path = path.Split(',').Select(x => int.Parse(x)), userId = user.Id}).ToArray();

            return ConvertToPermission(result.FirstOrDefault());
        }

        private static void AssignEntityPermission(this IMediaRepository repository, IMedia entity, char permission, int userId) {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            using (var trans = db.GetTransaction())
            {
                db.Execute("DELETE FROM umbracoUser2NodePermission WHERE nodeId=@nodeId AND permission=@permission AND userId=@userId",
                    new
                    {
                        nodeId = entity.Id,
                        permission = permission.ToString(CultureInfo.InvariantCulture),
                        userId = userId
                    });

                var action = new User2NodePermissionDto
                {
                    NodeId = entity.Id,
                    Permission = permission.ToString(CultureInfo.InvariantCulture),
                    UserId = userId
                };

                db.Insert(action);

                trans.Complete();
            }
        }

        private static IEnumerable<EntityPermission> ConvertToPermissionList(IEnumerable<User2NodePermissionDto> result)
        {
            var permissions = new List<EntityPermission>();
            var nodePermissions = result.GroupBy(x => x.NodeId);
            foreach (var np in nodePermissions)
            {
                var userPermissions = np.GroupBy(x => x.UserId);
                foreach (var up in userPermissions)
                {
                    var perms = up.Select(x => x.Permission).ToArray();
                    permissions.Add(new EntityPermission(up.Key, up.First().NodeId, perms));
                }
            }
            return permissions;
        }

        public static EntityPermission GetDefaultPermission(this IUser user, int mediaId)
        {
            return new EntityPermission(user.Id, mediaId, user.DefaultPermissions.ToArray());
        }

        private static EntityPermission ConvertToPermission(User2NodePermissionDto result)
        {
            if (result == null) return null;

            EntityPermission permissions = new EntityPermission(result.UserId, result.NodeId, new[] { result.Permission });
            return permissions;
        }


        private static void DeleteEntityPermission(this IMediaRepository repository, IMedia entity)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            using (var trans = db.GetTransaction())
            {
                db.Execute("DELETE FROM umbracoUser2NodePermission WHERE nodeId=@nodeId",
                    new
                    {
                        nodeId = entity.Id
                    });

                trans.Complete();
            }

        }
    }
}