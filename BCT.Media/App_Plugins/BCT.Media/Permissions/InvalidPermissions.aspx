﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../../../umbraco/masterpages/umbracoPage.Master" CodeBehind="InvalidPermissions.aspx.cs" Inherits="BCT.Media.InvalidPermissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <%
        int error = -1;
        int.TryParse(Request.QueryString["error"], out error);    
    %>
    <% if (error == 0) { %>
        <h3>Invalid media id.</h3>
    <% } else if (error == 1) { %>
        <h3>User does not have the permission to browse this item.</h3>
    <% } else if (error == 2) { %>
        <h3>User does not have the permission to edit this item.</h3>
    <% } %>
</asp:Content>
