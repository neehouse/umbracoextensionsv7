﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Trees;
using BCT.Media.Extensions;
using Umbraco.Core.Models.Membership;
using umbraco.BusinessLogic.Actions;
using System.Reflection;
using Umbraco.Core.IO;
using System.Collections;
using System.Threading.Tasks;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using BCT.Media.Delegate;
using Umbraco.Core.Models;
using Umbraco.Web.Models.Trees;


namespace BCT.Media.App_Start
{


    public class MediaEvents: ApplicationEventHandler
    {
        protected override void ApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            GlobalConfiguration.Configuration.MessageHandlers.Add(new WebApiHandler());

            // Tree Event Hooks
            TreeControllerBase.TreeNodesRendering += TreeControllerBase_TreeNodesRendering;
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;

            // MediaService Event Hooks
            MediaService.Saving += MediaService_Saving;
            MediaService.Deleting += MediaService_Deleting;
            MediaService.Moving += MediaService_Moving;
            MediaService.Deleted += MediaService_Deleted;
            MediaService.Moved += MediaService_Moved;
            MediaService.Saved += MediaService_Saved;
        }


        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;
            if (e.Menu != null && sender.TreeAlias == "media" && user.Id != 0)
            {
                var ms = ApplicationContext.Current.Services.MediaService;
                var media = ms.GetById(int.Parse(e.NodeId));

                EntityPermission user2NodePermissions = null;
                if (media != null)
                    user2NodePermissions = ms.GetUser2NodePermissions(media.Path, user);
                if (user2NodePermissions == null)
                    user2NodePermissions = user.GetDefaultPermission(-1);

                if (user2NodePermissions != null)
                {
                    if (!user2NodePermissions.AssignedPermissions.Contains("C"))
                        RemoveMenuAction(e.Menu, "create");

                    if (!user2NodePermissions.AssignedPermissions.Contains("D"))
                        RemoveMenuAction(e.Menu, "delete");

                    if (!user2NodePermissions.AssignedPermissions.Contains("M"))
                        RemoveMenuAction(e.Menu, "move");

                    if (!user2NodePermissions.AssignedPermissions.Contains("S"))
                        RemoveMenuAction(e.Menu, "sort");
                }
            }
        }

        void MediaService_Moved(IMediaService sender, Umbraco.Core.Events.MoveEventArgs<IMedia> e)
        {
            foreach (var movedItem in e.MoveInfoCollection)
            {
                var entity = movedItem.Entity;
                var parent = sender.GetParent(entity);

                if (parent != null)
                {
                    var parentPermissions = sender.GetNodePermissions(parent);
                    var us = ApplicationContext.Current.Services.UserService;


                    foreach (var parentPermission in parentPermissions)
                    {
                        var user = us.GetUserById(parentPermission.UserId);
                        foreach (var permission in parentPermission.AssignedPermissions)
                        {
                            try
                            {
                                sender.AssignMediaPermissions(entity, Convert.ToChar(permission), parentPermission.UserId);
                            }
                            catch { }
                        }
                    }
                }
            }
        }

        void MediaService_Saved(IMediaService sender, Umbraco.Core.Events.SaveEventArgs<IMedia> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                if (entity.IsNewEntity())
                {
                    var parent = sender.GetParent(entity);
                    if (parent != null)
                    {
                        var parentPermissions = sender.GetNodePermissions(parent);
                        var us = ApplicationContext.Current.Services.UserService;


                        foreach (var parentPermission in parentPermissions)
                        {
                            var user = us.GetUserById(parentPermission.UserId);
                            foreach (var permission in parentPermission.AssignedPermissions)
                            {
                                sender.AssignMediaPermissions(entity, Convert.ToChar(permission), parentPermission.UserId);
                            }
                        }
                    }
                }
            }
        }

        void MediaService_Deleted(IMediaService sender, Umbraco.Core.Events.DeleteEventArgs<Umbraco.Core.Models.IMedia> e)
        {
            try
            {
                foreach (var entity in e.DeletedEntities)
                {
                    sender.DeleteMediaPermissions(entity);
                }
            }
            catch { }
        }


        void MediaService_Moving(IMediaService sender, Umbraco.Core.Events.MoveEventArgs<Umbraco.Core.Models.IMedia> e)
        {
            var user  = UmbracoContext.Current.Security.CurrentUser;
            foreach( var movedItem in e.MoveInfoCollection) {
                var entity = movedItem.Entity;
                var permissions = sender.GetUser2NodePermissions(entity.Path, user);
                
                if (permissions == null)
                    permissions = user.GetDefaultPermission(entity.Id);
                
                if (!permissions.AssignedPermissions.Any(p => p == "M"))
                {
                    e.Cancel = true;
                }
            }
        }

        void MediaService_Saving(IMediaService sender, Umbraco.Core.Events.SaveEventArgs<IMedia> e)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;
            foreach (var entity in e.SavedEntities)
            {
                var parent = entity.Parent();
                EntityPermission permissions = null;
                if (parent != null)
                {
                    permissions = sender.GetUser2NodePermissions(parent.Path, user);
                }
                if (permissions == null)
                    permissions = user.GetDefaultPermission(entity.Id);

                if (!permissions.AssignedPermissions.Any(p => p == "A"))
                {
                    e.Cancel = true;
                    // delete?
                }

            }
        }

        void MediaService_Deleting(IMediaService sender, Umbraco.Core.Events.DeleteEventArgs<Umbraco.Core.Models.IMedia> e)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;
            foreach (var entity in e.DeletedEntities)
            {
                var permissions = sender.GetUser2NodePermissions(entity.Path, user);
                if (permissions == null)
                    permissions = user.GetDefaultPermission(entity.Id);
                
                if (!permissions.AssignedPermissions.Any(p => p == "D"))
                {
                    e.Cancel = true;
                }
            }
        }

        void TreeControllerBase_TreeNodesRendering(TreeControllerBase sender, TreeNodesRenderingEventArgs e)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;
            if (user.Id != 0 && sender.TreeAlias == "media")
            {
                try
                {
                    foreach (var node in e.Nodes)
                    {
                        IMediaService ms = Umbraco.Web.UmbracoContext.Current.Application.Services.MediaService;
                        IMedia media = ms.GetById(int.Parse(node.Id.ToString()));
                        string path = media.Path;

                        var permissions = ms.GetUser2NodePermissions(media.Path, user);
                        if (permissions == null)
                            permissions = user.GetDefaultPermission(media.Id);

                        if (!permissions.AssignedPermissions.Any(p => p == "F"))
                        {
                            e.Nodes.Remove(node);
                        }
                    }
                }
                catch { }
            } 
        }
        private static void RemoveMenuAction(MenuItemCollection menu, string action)
        {
            if (menu != null)
            {
                // If it has create, see if it needs it.
                if (menu.Items.Where(x => x.Alias == action).Any())
                {
                    menu.Items.RemoveAll(x => x.Alias == action);
                }
            }
        }
    }
}

