﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using umbraco.businesslogic;
using umbraco.interfaces;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using umbraco.BusinessLogic.Actions;
using umbraco.cms.presentation.Trees;
using umbraco.IO;
using Umbraco.Core;
using Umbraco.Core.Models.Membership;

namespace BCT.Media.Trees
{
    [Tree("users", "mediaPermissions", "Media Permissions", sortOrder: 3)]
    public class MediaPermissions : BaseTree
    {

        public MediaPermissions(string application) : base(application) { }

        /// <summary>
        /// don't allow any actions on this tree
        /// </summary>
        /// <param name="actions"></param>
        protected override void CreateAllowedActions(ref List<IAction> actions)
        {
            actions.Clear();
        }

        /// <summary>
        /// no actions should be able to be performed on the parent node except for refresh
        /// </summary>
        /// <param name="actions"></param>
        protected override void CreateRootNodeActions(ref List<IAction> actions)
        {
            actions.Clear();
            actions.Add(ActionRefresh.Instance);
        }

        public override void Render(ref XmlTree tree)
        {
            var totalRecords = 0;
            foreach (User user in ApplicationContext.Current.Services.UserService.GetAll(0,int.MaxValue, out totalRecords))
            {
                if (user.Id > 0 &&  user.IsApproved)
                {
                    XmlTreeNode node = XmlTreeNode.Create(this);
                    node.NodeID = user.Id.ToString();
                    node.Text = user.Name;
                    node.Action = "javascript:openMediaPermissions('" + user.Id.ToString() + "');";
                    node.Icon = "icon-users";

                    OnBeforeNodeRender(ref tree, ref node, EventArgs.Empty);
                    if (node != null)
                    {
                        tree.Add(node);
                        OnAfterNodeRender(ref tree, ref node, EventArgs.Empty);
                    }
                    
                }
            }
        }

        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Text = "Media Permissions";
        }

        public override void RenderJS(ref StringBuilder Javascript)
        {
            Javascript.Append(
                @"
function openMediaPermissions(id) {
	UmbClientMgr.contentFrame('../App_Plugins/BCT.Media/Permissions/PermissionEditor.aspx?id=' + id);
}
");
        }

    }
}
