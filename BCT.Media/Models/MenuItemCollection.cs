﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Models.Trees;

namespace BCT.Media.Models
{
    public class MenuItemCollection : Umbraco.Web.Models.Trees.MenuItemCollection
    {
        private  MenuItemList _menuItems = new MenuItemList();

        public MenuItemCollection()
        {           
        }

        public MenuItemCollection(IEnumerable<MenuItem> items)
        {
            Items = new MenuItemList(items);
        }
        
        /// <summary>
        /// Sets the default menu item alias to be shown when the menu is launched - this is optional and if not set then the menu will just be shown normally.
        /// </summary>
        public string DefaultMenuAlias { get; set; }

        /// <summary>
        /// The list of menu items
        /// </summary>
        /// <remarks>
        /// We require this so the json serialization works correctly
        /// </remarks>
        public new MenuItemList Items {
            get { return _menuItems; }
            set { _menuItems = value; }
        }
    }
}