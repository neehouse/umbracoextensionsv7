(function () {
    angular.module("umbraco").controllerProvider.register("BCT.Themes.TestTheme.SettingsController",
        function ($scope) {
            $scope.settingsEditor.loading = false;
        }
    );
})();