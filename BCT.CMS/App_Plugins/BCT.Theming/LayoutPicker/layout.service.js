(function () {
    angular.module('umbraco').factory('layoutService',
        function ($q, $http, umbDataFormatter, umbRequestHelper) {
            var servicePath = "../Umbraco/backoffice/BCT/Themes/";
            return {
                getLayout: function (nodeId, layout) {
                    var deferred = $q.defer();
                    var data = { nodeId: nodeId, layout: layout };
                    $http.get(servicePath + 'GetLayout', { params: data })
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        });
                    return deferred.promise;
                },
                getAllLayouts: function (nodeId) {
                    var deferred = $q.defer();
                    var data = { nodeId: nodeId };
                    $http.get(servicePath + 'GetAllLayouts', { params: data })
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        });
                    return deferred.promise;
                }
            }
        }
    );
})();