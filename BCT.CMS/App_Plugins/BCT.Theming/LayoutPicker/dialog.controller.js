(function () {
    angular.module("umbraco").controller("BCT.LayoutPicker.DialogController",
	    function ($scope, $routeParams, layoutService) {

	        init();

	        function init() {           
	            layoutService.getAllLayouts($routeParams.id)
	                .then(
	                    function (data) {
	                        $scope.layouts = data;
	                    }
	                );  
            };

	        $scope.selectLayout = function (layout) {
	            $scope.selectedLayout = layout;
	        };
	    }
    );
})();