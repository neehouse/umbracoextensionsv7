(function () {
    angular.module("umbraco").controller("BCT.DuplicateNameController",
        function ($rootScope, $scope, notificationsService, dialogService, $routeParams, editorState) {
            if ($routeParams.create == 'true') {

                $scope.content = editorState.current;

                $scope.$watch('content.name', function (newValue, oldValue) {
                    if ($scope.model.value == oldValue) {
                        $scope.model.value = newValue;
                    }
                });
            }
        }
    );
})();