(function () {
    angular.module('umbraco').factory('themeResource2',
        function ($q, $http, umbDataFormatter, umbRequestHelper) {
            var servicePath = "../Umbraco/BCT/Themes/";
            return {
                getTheme: function (path) {
                    var deferred = $q.defer();
                    $http.get(path.replace("~", "..") + '/theme.manifest')
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        });
                    return deferred.promise;
                },
                getAllThemes: function () {
                    var deferred = $q.defer();
                    $http.get(servicePath + 'GetAllTheme')
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        });
                    return deferred.promise;
                }
            }
        }
    );
})();