(function () {
    angular.module("umbraco").controller("BCT.ThemePicker.EditorController",
        function ($scope, dialogService, $rootScope, themeResource2, assetsService) {
            // get the themes from the service.
            //$scope.theme = { path: '~/themes/', name: "Test Theme", description: "description", image: "../App_Plugins/BCT.Directives/imageDropdown/images/NotAvailable.jpg" };

            init();

            function init() {
                // init scope
                initScope();
                initSettingsEditor();

                if ($scope.model.value.theme != undefined) {
                    themeResource.getTheme($scope.model.value.theme).then(function (data) {
                        $scope.theme = data;
                        if (data.settingsEditor != undefined) {
                            $scope.settingsEditor.loading = true;
                            // load js
                            assetsService
                                .load(data.settingsEditor.javascript, $rootScope)
                                .then(function () {
                                    if (data.settingsEditor.view != undefined) {
                                        $scope.settingsEditor.view = $scope.model.value.theme + '/' + data.settingsEditor.view;
                                    }
                                });
                            // load css
                            assetsService.load(data.settingsEditor.css, $rootScope);
                        } else {
                            $scope.settingsEditor = { loading: false, view: '' };
                        }
                    });
                }
            };

            function initScope() {
                if ($scope.model.value.theme == undefined) {
                    var val = { theme: '', settings: {} };
                    $scope.model.value.theme = val;
                }
            }

            function initSettingsEditor() {
                $scope.settingsEditor = { loading: false, view: '' };
            }

            $scope.clearTheme = function () {
                $scope.model.value.theme = '';
                $scope.theme = null;
                initSettingsEditor();
            };


            $scope.openThemePicker = function () {
                var pickerScope = $rootScope.$new();
                pickerScope.selectedTheme = $scope.model.value.theme;

                dialogService.open({
                    template: "../App_Plugins/BCT.PropertyEditors/ThemePicker/dialog.html",
                    scope: pickerScope,
                    show: true,
                    callback: function (theme) {
                        var val = {
                            theme: theme.path,
                            settings: {}
                        }

                        $scope.model.value.theme = theme.path;
                        init();
                        dialogService.closeAll();
                        // load theme configuration editor
                    }
                });
            };


        }
    );
})();