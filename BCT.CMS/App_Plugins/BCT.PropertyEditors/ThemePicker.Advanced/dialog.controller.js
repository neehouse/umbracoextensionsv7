(function () {
    angular.module("umbraco").controller("BCT.ThemePicker.DialogController",
	    function ($scope, themeResource2) {

	        init();

	        $scope.themes = [
                { path: '~/Themes/Custom/TestTheme', name: "Test Theme 0", description: "description", image: "../Themes/Custom/TestTheme/_theme/theme.jpg" },
                { path: '~/Themes/Custom/AnotherTheme', name: "Another Theme 0", description: "description", image: "../Themes/Custom/AnotherTheme/_theme/theme.jpg" }
            ];

	        function init() {
	            /*
	            themeResource.getThemes()
	            .then(
	            function (data) {
	            $scope.themes = data;
	            }
	            );
	            */
	        };

	        $scope.selectTheme = function (theme) {
	            $scope.selectedTheme = theme;
	        };
	    }
    );
})();