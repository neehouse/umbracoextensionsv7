﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCT.Theming.Models
{
    public class LayoutModel
    {
        public string name { get; set; }
        public string description { get; set; }
        public string filename { get; set; }
        public string image { get; set; }
    }
}