﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCT.Theming.Models
{
    public class ThemeModel
    {
        public string path { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string image { get; set; }
    }
}