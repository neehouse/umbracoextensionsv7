﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCT.Theming.Models
{
    public class SettingsEditorModel
    {
        public string view { get; set; }
        public List<string> javascript { get; set; }
        public List<string> css { get; set; }
    }
}