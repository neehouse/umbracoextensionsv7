﻿using BCT.Theming.App_Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Membership;
using Umbraco.Web;
using Umbraco.Web.Models.ContentEditing;
using Umbraco.Web.Models.Trees;

namespace BCT.Theming.Delegate
{
    public class WebApiHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;


            switch (request.RequestUri.AbsolutePath.ToLower()) {
                // Checks to see if the allowed content types have any associated templates in the theme, if not, it fails.  
                case "/umbraco/backoffice/umbracoapi/contenttype/getallowedchildren":
                    return FilterAllowedTypes(request, cancellationToken, user);                   
                default:
                    return base.SendAsync(request, cancellationToken);
            }
        }

        private Task<HttpResponseMessage> FilterAllowedTypes(HttpRequestMessage request, CancellationToken cancellationToken, IUser user)
        {
            return base.SendAsync(request, cancellationToken)
                .ContinueWith(task =>
                {
                    var response = task.Result;

                    var items = ((List<ContentTypeBasic>)(((System.Net.Http.ObjectContent)(response.Content)).Value));

                    var nodeId = 0;

                    int.TryParse(request.RequestUri.ParseQueryString().Get("contentId"), out nodeId);

                    if (nodeId > 0)
                    {
                        // Get the content node
                        var cs = ApplicationContext.Current.Services.ContentService;
                        var content = cs.GetById(nodeId);
                        var website = AncestorOrSelf(content, "Website");

                        if (website != null)
                        {
                            // Get the site theme to test the folders
                            var siteTheme = website.GetValue<string>("siteTheme");

                            if (string.IsNullOrEmpty(siteTheme))
                                siteTheme = Config.DefaultTheme;  

                            var files = Directory.GetFiles(Umbraco.Core.IO.IOHelper.MapPath(siteTheme) + "/Views");

                            if (Directory.Exists(Umbraco.Core.IO.IOHelper.MapPath(siteTheme.Substring(0, siteTheme.LastIndexOf("/")) + "/_Common/Views")))
                            {
                                var commonfiles = Directory.GetFiles(Umbraco.Core.IO.IOHelper.MapPath(siteTheme.Substring(0, siteTheme.LastIndexOf("/")) + "/_Common/Views"));
                                files = files.Union(commonfiles).ToArray();
                            }

                            var definedAliases = new List<string>();

                            foreach (var file in files)
                            {
                                var fi = new FileInfo(file);
                                definedAliases.Add(fi.Name.Substring(0, fi.Name.LastIndexOf(".")).ToLower());
                            }

                            var cts = ApplicationContext.Current.Services.ContentTypeService;
                            var contentTypes = cts.GetAllContentTypes();

                            var itemsToRemove = new List<ContentTypeBasic>();

                            foreach (ContentTypeBasic item in items)
                            {
                                var contentTypeId = 0;
                                int.TryParse(item.Id.ToString(), out contentTypeId);

                                var contentType = contentTypes.Where(x => x.Id == contentTypeId).FirstOrDefault();
                                var templateAlias = string.Empty;
                                if (contentType.DefaultTemplate != null)
                                    templateAlias = contentType.DefaultTemplate.Alias;

                                // if we have no theme templates that match the allowed templates, but we do have allowed templates, remove the item.
                                if (!contentType.AllowedTemplates.Where(y => definedAliases.Contains(y.Alias.ToLower())).Any() && contentType.AllowedTemplates.Any())
                                {
                                    if (user.UserType.Alias == "admin")
                                    {
                                        item.Name = item.Name + " (No Themed Template)";
                                    }
                                    else
                                    {
                                        itemsToRemove.Add(item);
                                    }
                                }
                            }

                            foreach (var item in itemsToRemove)
                            {
                                items.Remove(item);
                            }

                           ((System.Net.Http.ObjectContent)(response.Content)).Value = items;
                        }
                    }

                    return response;
                });
        }

        private IContent AncestorOrSelf(IContent content, string contentTypeAlias)
        {
            if (content.ContentType.Alias == contentTypeAlias)
            {
                return content;
            }
            else
            {
                foreach (var ancestor in content.Ancestors())
                {
                    if (ancestor.ContentType.Alias == contentTypeAlias)
                        return ancestor;
                }
            }
            return default(IContent);
        }
    }
}