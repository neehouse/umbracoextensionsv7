﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCT.Theming.App_Config
{
    public static class Config
    {
        public static string DefaultTheme = "~/";

        public static string ThemesRoot = "~/Themes/";
    }
}
