﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using umbraco;
using umbraco.cms.businesslogic.macro;
using Umbraco.Core;
using Umbraco.Core.IO;
using Umbraco.Web;
using Umbraco.Web.Templates;
using System.Web.Mvc.Html;
using Umbraco.Core.Models;

namespace BCT.Theming.Extensions
{
    /// <summary>
    /// Summary description for UmbracoHelperExtensions
    /// </summary>
    public static class Helpers
    {
        #region RenderMacro Theme Helpers - Not relevant with onMacroRendering Events

        //public static IHtmlString RenderMacro(this UmbracoHelper umbracoHelper, string alias, bool themed)
        //{
        //    return umbracoHelper.ThemedMacro(alias);
        //}

        //public static IHtmlString RenderMacro(this UmbracoHelper umbracoHelper, string alias, bool themed, object parameters)
        //{
        //    return umbracoHelper.ThemedMacro(alias, parameters);
        //}

        //public static IHtmlString RenderMacro(this UmbracoHelper umbracoHelper, string alias, bool themed, IDictionary<string, object> parameters = null)
        //{
        //    return umbracoHelper.ThemedMacro(alias, parameters);
        //}

        //public static IHtmlString ThemedMacro(this UmbracoHelper umbracoHelper, string alias)
        //{
        //    return umbracoHelper.ThemedMacro(alias, new { });
        //}

        //public static IHtmlString ThemedMacro(this UmbracoHelper umbracoHelper, string alias, object parameters)
        //{
        //    return umbracoHelper.ThemedMacro(alias, parameters.ToDictionary<object>());
        //}

        //public static IHtmlString ThemedMacro(this UmbracoHelper umbracoHelper, string alias, IDictionary<string, object> parameters)
        //{
        //    if (alias == null) throw new ArgumentNullException("alias");

        //    var umbracoContext = UmbracoContext.Current;

        //    var m = macro.GetMacro(alias);
        //    if (umbracoContext.PageId == null)
        //    {
        //        throw new InvalidOperationException("Cannot render a macro when UmbracoContext.PageId is null.");
        //    }
        //    if (umbracoContext.PublishedContentRequest == null)
        //    {
        //        throw new InvalidOperationException("Cannot render a macro when there is no current PublishedContentRequest.");
        //    }
        //    var macroProps = new Hashtable();
        //    foreach (var i in parameters)
        //    {
        //        //TODO: We are doing at ToLower here because for some insane reason the UpdateMacroModel method of macro.cs 
        //        // looks for a lower case match. WTF. the whole macro concept needs to be rewritten.
        //        macroProps.Add(i.Key.ToLower(), i.Value);
        //    }

        //    // check the file for a 'Themed' version.
        //    var app = ApplicationContext.Current;
        //    var theme = umbracoContext.PublishedContentRequest.PublishedContent.GetRecursiveValue("siteTheme");
        //    if (!theme.IsNullOrWhiteSpace())
        //    {
        //        var pfs = new PhysicalFileSystem(theme);

        //        switch (m.Model.MacroType)
        //        {
        //            case MacroTypes.PartialView:
        //            case MacroTypes.Script:
        //                var ScriptName = umbracoContext.HttpContext.Server.MapPath(theme + m.Model.ScriptName.TrimStart("~"));
        //                if (pfs.FileExists(ScriptName))
        //                    m.Model.ScriptName = theme + m.Model.ScriptName.TrimStart("~");
        //                break;
        //            case MacroTypes.UserControl:
        //                var TypeName = umbracoContext.HttpContext.Server.MapPath(theme + m.Model.TypeName.TrimStart("~"));
        //                if (pfs.FileExists(TypeName))
        //                    m.Model.TypeName = theme + m.Model.TypeName.TrimStart("~");
        //                break;
        //            case MacroTypes.XSLT:
        //                var Xslt = umbracoContext.HttpContext.Server.MapPath(theme + "/Xslt/" + m.Model.Xslt);
        //                if (pfs.FileExists(Xslt))
        //                    m.Model.Xslt = (theme + "/xslt/" + m.Model.Xslt).Replace("/", "\\").Replace("~", "..");
        //                break;
        //        }
        //    }
        //    else // try Common
        //    {

        //    }
        //    var macroControl = m.renderMacro(macroProps,
        //        ((Hashtable)(umbracoContext.HttpContext.Items["pageElements"])),
        //        umbracoContext.PageId.Value);

        //    string html;
        //    if (macroControl is LiteralControl)
        //    {
        //        // no need to execute, we already have text
        //        html = (macroControl as LiteralControl).Text;
        //    }
        //    else
        //    {
        //        var containerPage = new FormlessPage();
        //        containerPage.Controls.Add(macroControl);

        //        using (var output = new StringWriter())
        //        {
        //            // .Execute() does a PushTraceContext/PopTraceContext and writes trace output straight into 'output'
        //            // and I do not see how we could wire the trace context to the current context... so it creates dirty
        //            // trace output right in the middle of the page.
        //            //
        //            // The only thing we can do is fully disable trace output while .Execute() runs and restore afterwards
        //            // which means trace output is lost if the macro is a control (.ascx or user control) that is invoked
        //            // from within Razor -- which makes sense anyway because the control can _not_ run correctly from
        //            // within Razor since it will never be inserted into the page pipeline (which may even not exist at all
        //            // if we're running MVC).
        //            //
        //            // I'm sure there's more things that will get lost with this context changing but I guess we'll figure 
        //            // those out as we go along. One thing we lose is the content type response output.
        //            // http://issues.umbraco.org/issue/U4-1599 if it is setup during the macro execution. So 
        //            // here we'll save the content type response and reset it after execute is called.

        //            var contentType = umbracoContext.HttpContext.Response.ContentType;
        //            var traceIsEnabled = containerPage.Trace.IsEnabled;
        //            containerPage.Trace.IsEnabled = false;
        //            umbracoContext.HttpContext.Server.Execute(containerPage, output, false);
        //            containerPage.Trace.IsEnabled = traceIsEnabled;
        //            //reset the content type
        //            umbracoContext.HttpContext.Response.ContentType = contentType;

        //            //Now, we need to ensure that local links are parsed
        //            html = TemplateUtilities.ParseInternalLinks(output.ToString());
        //        }
        //    }

        //    return new HtmlString(html);
        //}

        //internal static IDictionary<string, TVal> ToDictionary<TVal>(this object o, params string[] ignoreProperties)
        //{
        //    if (o != null)
        //    {
        //        var props = TypeDescriptor.GetProperties(o);
        //        var d = new Dictionary<string, TVal>();
        //        foreach (var prop in props.Cast<PropertyDescriptor>().Where(x => !ignoreProperties.Contains(x.Name)))
        //        {
        //            var val = prop.GetValue(o);
        //            if (val != null)
        //            {
        //                d.Add(prop.Name, (TVal)val);
        //            }
        //        }
        //        return d;
        //    }
        //    return new Dictionary<string, TVal>();
        //}

        #endregion

        #region Partial View Theming Support

        public static IHtmlString RenderPartial(this HtmlHelper htmlHelper, string partialViewName, object model = null, bool themed = false, int cachedSeconds = 0,
                                        bool cacheByPage = false, bool cacheByMember = false, ViewDataDictionary viewData = null)
        {
            return htmlHelper.Partial(partialViewName, model, themed, cachedSeconds, cacheByPage, cacheByMember, viewData);
        }

        public static IHtmlString Partial(this HtmlHelper htmlHelper, string partialViewName, object model = null, bool themed = false, int cachedSeconds = 0,
                                                bool cacheByPage = false, bool cacheByMember = false, ViewDataDictionary viewData = null)
        {
            if (themed)
                return htmlHelper.ThemedPartial(partialViewName, model, cachedSeconds, cacheByPage, cacheByMember, viewData);
            else
                return htmlHelper.CachedPartial(partialViewName, model, cachedSeconds, cacheByPage, cacheByMember, viewData);
        }

        public static IHtmlString RenderThemedPartial(this HtmlHelper htmlHelper, string partialViewName, object model = null, int cachedSeconds = 0, 
                                                bool cacheByPage = false, bool cacheByMember = false, ViewDataDictionary viewData = null)
        {
            return htmlHelper.ThemedPartial(partialViewName, model, cachedSeconds, cacheByPage, cacheByMember, viewData);
        }


        public static IHtmlString ThemedPartial(this HtmlHelper htmlHelper, string partialViewName, object model = null, int cachedSeconds = 0, 
                                                bool cacheByPage = false, bool cacheByMember = false, ViewDataDictionary viewData = null)
        {
            var umbracoContext = UmbracoContext.Current;
            UmbracoHelper helper = new UmbracoHelper(umbracoContext);

            var Model = umbracoContext.PublishedContentRequest.PublishedContent;

            var siteTheme = HttpContext.Current.Items["BCT.Theming.SiteTheme"].ToString();
            var themeGroup = HttpContext.Current.Items["BCT.Theming.ThemeGroup"].ToString();
            var defaultTheme = HttpContext.Current.Items["BCT.Theming.DefaultTheme"].ToString();

            var partial = partialViewName;

            //var partialExtensions = new string[] {".cshtml",".vbhtml", ".ascx"};
            if (TryThemedPartial(siteTheme, partialViewName))
            {
                partial = siteTheme + "/Views/Partials/" + partialViewName + ".cshtml";
                goto found;
            }

            if (TryThemedPartial(themeGroup, partialViewName))
            {
                partial = themeGroup + "/Views/Partials/" + partialViewName + ".cshtml";
                goto found;
            }

            if (TryThemedPartial(defaultTheme, partialViewName))
            {
                partial = defaultTheme + "/Views/Partials/" + partialViewName + ".cshtml";
                goto found;
            }

            found:
            if (cachedSeconds > 0)
            {
                return htmlHelper.CachedPartial(partial, model: model, cachedSeconds: cachedSeconds, cacheByPage: cacheByPage, cacheByMember: cacheByMember, viewData: viewData);
            }
            else
            {
                return htmlHelper.Partial(partial, model: model, viewData: viewData);
            }
        }

        #endregion

        #region IPublishedContent Helpers

        [Obsolete("Use GetThemedLayout instead.",false)]
        public static string GetLayout(this HtmlHelper html, string defaultLayout)
        {
            return GetThemedLayout(html, defaultLayout);
        }

        public static string GetThemedLayout(this HtmlHelper html, string defaultLayout)
        {
            var umbracoContext = UmbracoContext.Current;
            var content = umbracoContext.PublishedContentRequest.PublishedContent;

            var layout = defaultLayout;

            if (layout.StartsWith("__") && content.HasValue("layout"))
            {
                layout = content.GetPropertyValue<string>("layout");
            }

            var siteTheme = HttpContext.Current.Items["BCT.Theming.SiteTheme"].ToString();
            var themeGroup = HttpContext.Current.Items["BCT.Theming.ThemeGroup"].ToString();
            var defaultTheme = HttpContext.Current.Items["BCT.Theming.DefaultTheme"].ToString();

            if (TryThemedView(siteTheme, layout))
            {
                return siteTheme + "/Views/" + layout;
            }

            if (TryThemedView(themeGroup, layout))
            {
                return themeGroup + "/Views/" + layout;
            }

            if (TryThemedView(defaultTheme, defaultLayout))
            {
                return defaultTheme + "/Views/" + layout;
            }

            if (TryThemedView(siteTheme, defaultLayout))
            {
                return siteTheme + "/Views/" + defaultLayout;
            }

            if (TryThemedView(themeGroup, defaultLayout))
            {
                return themeGroup + "/Views/" + defaultLayout;
            }

            if (TryThemedView(defaultTheme, defaultLayout))
            {
                return defaultTheme + "/Views/" + defaultLayout;
            }

            return defaultLayout;
        }

        private static bool TryThemedDirectory(string theme, string directory)
        {
            var themePath = Umbraco.Core.IO.IOHelper.MapPath(theme + directory);
            return Directory.Exists(themePath);
        }

        private static bool TryThemedFile(string theme, string directory, string file)
        {
            var themePath = Umbraco.Core.IO.IOHelper.MapPath(theme + directory);
            return System.IO.File.Exists(themePath + file);
        }

        private static bool TryThemedPartial(string theme, string partial)
        {
            if (TryThemedDirectory(theme, "/Views/Partials/"))
            {
                return TryThemedFile(theme, "/Views/Partials/", partial + ".cshtml");
            }
            return false;
        }

        private static bool TryThemedView(string theme, string view)
        {
            if (TryThemedDirectory(theme, "/Views/"))
            {
                return TryThemedFile(theme , "/Views/", view);
            }
            return false;
        }


        #endregion

        #region URL Helpers
        public static string SmartUrl(this UmbracoHelper umbracoHelper, int contentId)
        {
            var item = umbracoHelper.TypedContent(contentId);
            return item.SmartUrl();
        }
        public static string SmartUrl(this IPublishedContent item)
        {
            var uc = UmbracoContext.Current;
            var link = "#";
            var linkId = 0;
            switch (item.DocumentTypeAlias)
            {
                case "NavigationExternalLink":
                    link = item.GetPropertyValue<string>("navigationURL");
                    break;
                case "ExternalLink":
                    link = item.GetPropertyValue<string>("link");
                    break;
                case "MediaLink":
                    if (int.TryParse(item.GetPropertyValue("link").ToString(), out linkId))
                    {
                        link = uc.MediaCache.GetById(linkId).GetPropertyValue<string>("umbracoFile");
                    }
                    break;
                case "NavigationLink":
                    if (int.TryParse(item.GetPropertyValue("navigationPage").ToString(), out linkId))
                    {
                        link = uc.ContentCache.GetById(linkId).Url;
                    }
                    break;
                case "InternalLink":
                    if (int.TryParse(item.GetPropertyValue("link").ToString(), out linkId))
                    {
                        link = uc.ContentCache.GetById(linkId).Url;
                    }
                    break;
                default:
                    link = item.Url;
                    break;
            }
            return link;
        }

        #endregion
    }

    internal class FormlessPage : Page
    {
        public override void VerifyRenderingInServerForm(Control control) { }
    }
}