﻿using BCT.Theming.App_Config;
using BCT.Theming.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Umbraco.Core.IO;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;
using Umbraco.Core;
using System.Text.RegularExpressions;

namespace BCT.Theming.Controllers
{
    [PluginController("BCT")]
    [IsBackOffice]
    public class ThemesController : UmbracoAuthorizedApiController
    {
        const string NotAvailableImage = "../App_Plugins/BCT.Theming/images/NotAvailable.jpg";

        [System.Web.Http.HttpGet]
        public ThemeModel GetTheme(string path)
        {
            var themesRootPath = IOHelper.MapPath(Config.ThemesRoot).ToLower();
            var themePath = IOHelper.MapPath(path).ToLower();
            return GetManifest(themePath, themesRootPath);
        }
        
        [System.Web.Http.HttpGet]
        public List<ThemeModel> GetAllThemes()
        {
            var themes = new List<ThemeModel>();
            var themesRootPath = IOHelper.MapPath(Config.ThemesRoot).ToLower();

            if (!Directory.Exists(themesRootPath)) return themes;
            foreach (var directory in Directory.GetDirectories(themesRootPath))
            {
                var di = new DirectoryInfo(directory);
                if (!(di.Name == "Common" || di.Name.StartsWith("_") || di.Name.StartsWith(".")))
                {
                    themes.AddRange(
                        Directory.GetDirectories(directory)
                            .Select(themePath => new {themePath, themeInfo = new DirectoryInfo(themePath)})
                            .Where(
                                t =>
                                    t.themeInfo.Name != "Common" && !t.themeInfo.Name.StartsWith("_") &&
                                    !t.themeInfo.Name.StartsWith("."))
                            .Select(t => GetManifest(t.themePath.ToLower(), themesRootPath.ToLower()))
                            .Where(manifest => manifest != null));
                }
            }

            return themes;
        }

        public LayoutModel GetLayout(int nodeId, string layout)
        {
            var content = ApplicationContext.Services.ContentService.GetById(nodeId);
            var website = content.ContentType.Alias == "Website" ? content : ApplicationContext.Current.Services.ContentService.GetAncestors(content).LastOrDefault(x => x.ContentType.Alias == "Website");

            var siteTheme = Config.DefaultTheme;

            if (!string.IsNullOrWhiteSpace(website?.GetValue<string>("siteTheme")))
                siteTheme = website.GetValue<string>("siteTheme");

            var layoutPath = IOHelper.MapPath(siteTheme + "/Views/" + layout);

            if (!File.Exists(layoutPath)) return new LayoutModel() {name = "Default Layout", image = NotAvailableImage};

            var fi = new FileInfo(layoutPath);
            var filename = fi.Name.Substring(2, fi.Name.LastIndexOf(".", StringComparison.Ordinal) - 2);
            var imagePath = siteTheme + "/Images/Layouts/__" + filename + ".jpg";

            imagePath = File.Exists(IOHelper.MapPath(imagePath)) ? imagePath.Replace("~/", "../") : NotAvailableImage;

            var name = char.ToUpper(filename[0]) + Regex.Replace(filename.Substring(1), @"([A-Z0-9])", @" $1");

            return new LayoutModel() { name = name, filename = fi.Name, image = imagePath };
        }

        public List<LayoutModel> GetAllLayouts(int nodeId)
        {
            var layouts = new List<LayoutModel>();
            try
            {
                var content = ApplicationContext.Services.ContentService.GetById(nodeId);
                var website = content.ContentType.Alias == "Website" ? content : ApplicationContext.Current.Services.ContentService.GetAncestors(content).LastOrDefault(x => x.ContentType.Alias == "Website");

                var siteTheme = Config.DefaultTheme;

                if (!string.IsNullOrWhiteSpace(website?.GetValue<string>("siteTheme")))
                    siteTheme = website.GetValue<string>("siteTheme");

                // TODO Lookup template and get the layout based on that.
                layouts.Add(new LayoutModel() { name = "Default Layout", image = NotAvailableImage });

                var themePath = IOHelper.MapPath(siteTheme + "/Views");

                if (Directory.Exists(themePath))
                {
                    foreach (var file in Directory.GetFiles(themePath))
                    {
                        var fi = new FileInfo(file);
                        if (!fi.Name.StartsWith("__")) continue;

                        var filename = fi.Name.Substring(2, fi.Name.LastIndexOf(".", StringComparison.Ordinal) - 2);
                        var imagePath = siteTheme + "/Images/Layouts/__" + filename + ".jpg";

                        imagePath = File.Exists(IOHelper.MapPath(imagePath)) ? imagePath.Replace("~/", "../") : NotAvailableImage;

                        var name = char.ToUpper(filename[0]) + Regex.Replace(filename.Substring(1), @"([A-Z0-9])", @" $1");

                        layouts.Add(new LayoutModel() { name = name, filename = fi.Name, image = imagePath });
                    }
                }
            }
            catch
            {
                // ignored
            }

            return layouts;
        }

        private ThemeModel GetManifest(string themePath, string themesRootPath)
        {
            var manifest = new ThemeModel();

            if (!Directory.Exists(themePath)) return manifest;

            var themeInfo = new DirectoryInfo(themePath);
            var themeUrlPath = themePath.ToLower().Replace(themesRootPath.ToLower(), Config.ThemesRoot.ToLower()).Replace("\\", "/");
            var manifestPath = themePath + "\\theme.manifest";

            if (File.Exists(manifestPath))
            {
                using (StreamReader r = new StreamReader(manifestPath))
                {
                    string json = r.ReadToEnd();
                    manifest = JsonConvert.DeserializeObject<ThemeModel>(json);
                }
            }
            if (string.IsNullOrWhiteSpace(manifest.name))
                manifest.name = themeInfo.Name + " Theme";

            if (string.IsNullOrWhiteSpace(manifest.description))
                manifest.description = themeUrlPath;

            if (!string.IsNullOrWhiteSpace(manifest.image))
            {
                if (!File.Exists(themePath + "/" + manifest.image))
                {
                    manifest.image = string.Empty;
                }
                else
                {
                    manifest.image = themeUrlPath.ToLower().Replace("~/","../") + "/" + manifest.image;
                }
            }

            if (string.IsNullOrWhiteSpace(manifest.image))
            {
                var image = themePath + "\\_theme\\theme.jpg";

                if (!File.Exists(image))
                {
                    image = NotAvailableImage;
                }

                var imageUrlPath = image.ToLower().Replace(themesRootPath.ToLower(), Config.ThemesRoot.ToLower()).Replace("\\", "/");

                manifest.image = imageUrlPath.Replace("~/", "../").ToLower();
            }

            if (string.IsNullOrWhiteSpace(manifest.path))
                manifest.path = themeUrlPath.ToLower();

            return manifest;
        }
    }
}


