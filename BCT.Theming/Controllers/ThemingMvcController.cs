﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Core;

namespace BCT.Theming.Controllers
{
    public class ThemingMvcController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var template = ControllerContext.RouteData.Values["action"].ToString();

            var siteTheme = HttpContext.Items["BCT.Theming.SiteTheme"].ToString();
            var themeGroup = HttpContext.Items["BCT.Theming.ThemeGroup"].ToString();
            var defaultTheme = HttpContext.Items["BCT.Theming.DefaultTheme"].ToString();


            if (!siteTheme.IsNullOrWhiteSpace() && EnsurePhsyicalViewExists(siteTheme + "/Views/" + template + ".cshtml"))
                return View(siteTheme + "/Views/" + template + ".cshtml", model);
            else if (!themeGroup.IsNullOrWhiteSpace() && EnsurePhsyicalViewExists(themeGroup + "/Views/" + template + ".cshtml"))
                return View(themeGroup + "/Views/" + template + ".cshtml", model);
            else if (EnsurePhsyicalViewExists(defaultTheme + "/Views/" + template + ".cshtml"))
                return View(defaultTheme + "/Views/" + template + ".cshtml", model);
            else if (EnsurePhsyicalViewExists(template))
                return View(template, model);
            else
                return Content("");
        }
    }
}
