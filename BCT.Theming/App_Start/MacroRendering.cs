﻿using BCT.Theming.App_Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Macros;
using Umbraco.Core;
using Umbraco.Core.IO;
using Umbraco.Web;
using umbraco.cms.businesslogic.macro;

namespace BCT.Theming.App_Start
{
    public class MacroRendering : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            umbraco.macro.MacroRendering += macro_MacroRendering;
        }

        void macro_MacroRendering(umbraco.macro sender, umbraco.MacroRenderingEventArgs e)
        {
            var umbracoContext = UmbracoContext.Current;

            var siteTheme = string.Empty;
            var themeGroup = string.Empty;
            var defaultTheme = string.Empty;

            try
            {
                siteTheme = HttpContext.Current.Items["BCT.Theming.SiteTheme"].ToString();
                themeGroup = HttpContext.Current.Items["BCT.Theming.ThemeGroup"].ToString();
                defaultTheme = HttpContext.Current.Items["BCT.Theming.DefaultTheme"].ToString();
            }
            catch
            {
                var pcr = umbracoContext.PublishedContentRequest;
                int pageId = 0;

                int.TryParse(HttpContext.Current.Items["pageID"].ToString(), out pageId);

                var content = umbracoContext.ContentCache.GetById(pageId);

                if (content != null)
                {
                    siteTheme = content.AncestorsOrSelf("Website").LastOrDefault().GetPropertyValue<string>("siteTheme");

                    if (siteTheme.IsNullOrWhiteSpace())
                    {
                        siteTheme = Config.DefaultTheme;
                    }

                    themeGroup = siteTheme.Substring(0, siteTheme.LastIndexOf("/")) + "/_Common";
                }
            }

            if (TryTheme(siteTheme, sender))
                return;

            if (TryTheme(themeGroup, sender))
                return;

            if (TryTheme(Config.DefaultTheme, sender))
                return;
        }

        private bool TryTheme(string theme, umbraco.macro sender)
        {
            var app = ApplicationContext.Current;
            var umbracoContext = UmbracoContext.Current;

            var pfs = new PhysicalFileSystem(theme);

            switch (sender.Model.MacroType)
            {
                case MacroTypes.PartialView:
                case MacroTypes.Script:
                    var ScriptName = umbracoContext.HttpContext.Server.MapPath(theme + sender.Model.ScriptName.TrimStart("~"));
                    if (pfs.FileExists(ScriptName))
                    {
                        sender.Model.ScriptName = theme + sender.Model.ScriptName.TrimStart("~");
                        return true;
                    }
                    break;
                case MacroTypes.UserControl:
                    var TypeName = umbracoContext.HttpContext.Server.MapPath(theme + sender.Model.TypeName.TrimStart("~"));
                    if (pfs.FileExists(TypeName))
                    {
                        sender.Model.TypeName = theme + sender.Model.TypeName.TrimStart("~");
                        return true;
                    }
                    break;
                case MacroTypes.XSLT:
                    var Xslt = umbracoContext.HttpContext.Server.MapPath(theme + "/Xslt/" + sender.Model.Xslt);
                    if (pfs.FileExists(Xslt))
                    {
                        sender.Model.Xslt = (theme + "/xslt/" + sender.Model.Xslt).Replace("/", "\\").Replace("~", "..");
                        return true;
                    }
                    break;
            }
            return false;
        }
    }
}