﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.BusinessLogic;
using umbraco.cms.presentation.Trees;
using umbraco.interfaces;
using umbraco.presentation.masterpages;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Trees;
using Umbraco.Web.UI.Pages;
using Umbraco.Web.Models.Trees;
using BCT.Theming.Delegate;

using umbraco.BusinessLogic.Actions;
using BCT.Theming.App_Config;

namespace BCT.Theming.App_Start
{
    public class AllowedDocuments : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
// removed for now due to certain child types, such as links/redirects/etc.

            
//            GlobalConfiguration.Configuration.MessageHandlers.Add(new WebApiHandler());
            
            // remove create option if no child-types are available.
 //           TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;

            
        }

        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            if (e.Menu != null && sender.TreeAlias == "content" && !isAdmin)
            {
                if (e.Menu.Items.Where(x => x.Alias == "create").Any())
                {
                    var cs = ApplicationContext.Current.Services.ContentService;
                    var content = cs.GetById(int.Parse(e.NodeId));

                    if (!content.ContentType.AllowedContentTypes.Any())
                    {
                        var website = AncestorOrSelf(content, "Website");

                        if (website != null)
                        {
                            // Get the site theme to test the folders
                            var siteTheme = website.GetValue<string>("siteTheme");

                            if (string.IsNullOrEmpty(siteTheme) || !Directory.Exists(Umbraco.Core.IO.IOHelper.MapPath(siteTheme) + "/Views"))
                                siteTheme = Config.DefaultTheme;

                            var files = Directory.GetFiles(Umbraco.Core.IO.IOHelper.MapPath(siteTheme) + "/Views");

                            if (Directory.Exists(Umbraco.Core.IO.IOHelper.MapPath(siteTheme.Substring(0, siteTheme.LastIndexOf("/")) + "/_Common/Views")))
                            {
                                var commonfiles = Directory.GetFiles(Umbraco.Core.IO.IOHelper.MapPath(siteTheme.Substring(0, siteTheme.LastIndexOf("/")) + "/_Common/Views"));
                                files = files.Union(commonfiles).ToArray();
                            }

                            var definedAliases = new List<string>();

                            // defined theme templates
                            foreach (var file in files)
                            {
                                var fi = new FileInfo(file);
                                definedAliases.Add(fi.Name.Substring(0, fi.Name.LastIndexOf(".")).ToLower());
                            }

                            var cts = ApplicationContext.Current.Services.ContentTypeService;
                            var contentTypes = cts.GetAllContentTypes();

                            foreach (var contentTypeBase in content.ContentType.AllowedContentTypes)
                            {
                                int contentTypeId = contentTypeBase.Id.Value;
                                var contentType = contentTypes.Where(x => x.Id == contentTypeId).FirstOrDefault();

                                var templateAlias = string.Empty;

                                if (contentType.DefaultTemplate != null)
                                    templateAlias = contentType.DefaultTemplate.Alias;

                                // if there are any matching templates, or a doc type does not have templates (resources), then exit and allow the create menu.
                                if (contentType.AllowedTemplates.Where(y => definedAliases.Contains(y.Alias.ToLower())).Any() || !contentType.AllowedTemplates.Any())
                                {
                                    return;
                                }
                            }
                        }

                        RemoveMenuAction(e.Menu, "create");
                    }
                }
            }
        }

        private void RemoveMenuAction(Umbraco.Web.Models.Trees.MenuItemCollection menu, string action)
        {
            if (menu != null)
            {
                // If it has create, see if it needs it.
                if (menu.Items.Where(x => x.Alias == action).Any())
                {
                    menu.Items.RemoveAll(x => x.Alias == action);
                }
            }
        }

        private bool isAdmin
        {
            get { return UmbracoContext.Current.Security.CurrentUser.UserType.Alias == "admin"; }
        }

        private IContent AncestorOrSelf(IContent content, string contentTypeAlias)
        {
            if (content.ContentType.Alias == contentTypeAlias)
            {
                return content;
            }
            else
            {
                foreach (var ancestor in content.Ancestors())
                {
                    if (ancestor.ContentType.Alias == contentTypeAlias)
                        return ancestor;
                }
            }
            return default(IContent);
        }

        private Control FindControlRecursive(Control control, string id)
        {
            if (control == null) return null;
            Control ctrl = control.FindControl(id);

            if (ctrl == null)
            {
                foreach (Control child in control.Controls)
                {
                    ctrl = FindControlRecursive(child, id);
                    if (ctrl != null) break;
                }
            }
            return ctrl;
        }

    }
}
