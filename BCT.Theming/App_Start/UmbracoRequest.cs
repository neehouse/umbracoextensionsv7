﻿using BCT.Theming.App_Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace BCT.Theming.App_Start
{
    public class UmbracoRequest : ApplicationEventHandler
    {
        protected override void ApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PublishedContentRequest.Prepared += PublishedContentRequest_Prepared;

            if (ConfigurationManager.AppSettings.AllKeys.Contains("BCT.Theming.DefaultTheme"))
                Config.DefaultTheme = ConfigurationManager.AppSettings["BCT.Theming.DefaultTheme"];

            if (ConfigurationManager.AppSettings.AllKeys.Contains("BCT.Theming.ThemesRoot"))
                Config.ThemesRoot = ConfigurationManager.AppSettings["BCT.Theming.ThemesRoot"];
        }

        void PublishedContentRequest_Prepared(object sender, EventArgs e)
        {
            // Get site theme.
            var pcr = (PublishedContentRequest)sender;

            if (pcr.HasPublishedContent)
            {
                var siteTheme = pcr.PublishedContent.GetPropertyValue<string>("siteTheme", true);

                if (siteTheme.IsNullOrWhiteSpace())
                {
                    siteTheme = Config.DefaultTheme;
                }

                var themeGroup = siteTheme.Substring(0, siteTheme.LastIndexOf("/")) + "/_Common";

                HttpContext.Current.Items.Add("BCT.Theming.SiteTheme", siteTheme);
                HttpContext.Current.Items.Add("BCT.Theming.ThemeGroup", themeGroup);
                HttpContext.Current.Items.Add("BCT.Theming.DefaultTheme", Config.DefaultTheme);
            }
        }
    }
}
