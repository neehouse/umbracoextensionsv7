﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.Routing;
using System.Web.Mvc.Html;
using System.Web.Routing;
using BCT.Theming.Controllers;

namespace BCT.Theming.App_Start
{
    /// <summary>
    /// Summary description for HomePageTemplateSwitcher
    /// </summary>
    public class DefaultRenderMvcController : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(ThemingMvcController));
        }
    }
}