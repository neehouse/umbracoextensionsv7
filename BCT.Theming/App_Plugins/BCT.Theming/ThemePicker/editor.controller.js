(function () {
    angular.module("umbraco").controller("BCT.ThemePicker.EditorController",
        function ($scope, dialogService, $rootScope, themeService, assetsService) {

            init();

            function init() {
                // init scope

                if (!isNullOrEmpty($scope.model.value)) {
                    themeService.getTheme($scope.model.value).then(function (data) {
                        $scope.theme = data;
                    });
                }
            };

            function isNullOrEmpty(val) {
                return angular.isUndefined(val) || val === null || val === '';
            }

            $scope.clearTheme = function () {
                $scope.model.value = '';
                $scope.theme = null;
            };


            $scope.openThemePicker = function () {
                var pickerScope = $rootScope.$new();
                pickerScope.selectedTheme = $scope.theme;

                dialogService.open({
                    template: "../App_Plugins/BCT.Theming/ThemePicker/dialog.html",
                    scope: pickerScope,
                    show: true,
                    callback: function (theme) {

                        $scope.model.value = theme.path;
                        $scope.theme = theme;
                        dialogService.closeAll();
                    }
                });
            };


        }
    );
})();