(function () {
    angular.module("umbraco").controller("BCT.ThemePicker.DialogController",
	    function ($scope, themeService) {

	        init();

	        function init() {           
	            themeService.getAllThemes()
	                .then(
	                    function (data) {
	                        $scope.themes = data;
	                    }
	                );  
            };

	        $scope.selectTheme = function (theme) {
	            $scope.selectedTheme = theme;
	        };
	    }
    );
})();