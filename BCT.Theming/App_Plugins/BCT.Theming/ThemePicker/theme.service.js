(function () {
    angular.module('umbraco').factory('themeService',
        function ($q, $http, umbDataFormatter, umbRequestHelper) {
            var servicePath = "../Umbraco/backoffice/BCT/Themes/";
            return {
                getTheme: function (path) {
                    var deferred = $q.defer();
                    var data = { path: path };
                    $http.get(servicePath + 'GetTheme', {params: data})
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        });
                    return deferred.promise;
                },
                getAllThemes: function () {
                    var deferred = $q.defer();
                    $http.get(servicePath + 'GetAllThemes')
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        });
                    return deferred.promise;
                }
            }
        }
    );
})();