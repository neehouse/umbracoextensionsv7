(function () {
    angular.module("umbraco").controller("BCT.LayoutPicker.EditorController",
        function ($scope, dialogService, $rootScope, $routeParams, layoutService, assetsService) {

            init();

            function init() {
                if (!isNullOrEmpty($scope.model.value)) {
                    layoutService.getLayout($routeParams.id, $scope.model.value).then(function (data) {
                        $scope.layout = data;
                    });
                }
            };

            function isNullOrEmpty(val) {
                return angular.isUndefined(val) || val === null || val === '';
            }

            $scope.clearLayout = function () {
                $scope.model.value = '';
                $scope.layout = null;
            };


            $scope.openLayoutPicker = function () {
                var pickerScope = $rootScope.$new();
                pickerScope.selectedLayout = $scope.layout;

                dialogService.open({
                    template: "../App_Plugins/BCT.Theming/LayoutPicker/dialog.html",
                    scope: pickerScope,
                    show: true,
                    callback: function (layout) {

                        $scope.model.value = layout.filename;
                        $scope.layout = layout;
                        dialogService.closeAll();
                    }
                });
            };


        }
    );
})();