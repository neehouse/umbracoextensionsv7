# Umbraco Extensions for v7 #

So, this is a bit of work that I did at a previous job, and I felt that the community could potentially benefit from them.  Some of the projects may get released as packages in the near future.

## BCT.CMS ##

This is simply a test site that I plugged the extensions into to see how things worked. u/p: admin/admin$123

## BCT.Common ##

Common things for manipulating the Umbraco Workflows.  Document Events, Extensions, and more.  Lot's of goodies to be found.  Especially the Impersonation classes for users and members..

## BCT.Media ##

Implements Media Permissions for Umbraco Users.  Acts basically the same as Content Permissions.  Does not have the dialog to manage permissions in the media section though.  This will get packaged soon.

## BCT.Theming ##

This project allows you to set themes on sites.  If the theme is set, all views (templates), macro partials, razor macros, xslt, user controls, and such get their directory changed.  Not a complicated project, but I implemented common theme groups, theme fall-back, and more.  There will be documentation coming later.

If you use the layout portion, there is a helper to include - Html.GetLayout("__DefaultLayout.cshtml") - This swaps to the selected user layout (prefixed with two underscores), or uses the defined default layout.

### Licensing ###

Released under MIT License.