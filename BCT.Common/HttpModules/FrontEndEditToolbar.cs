﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using umbraco;
using umbraco.BusinessLogic;
using Umbraco.Core.IO;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace HSC.Common.Rendering
{
    class FrontEndEditToolbarModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PostRequestHandlerExecute += context_PostRequestHandlerExecute;   
        }

        void context_PostRequestHandlerExecute(object sender, EventArgs e)
        {
            if (sender is HttpApplication)
            {
                using (var app = sender as HttpApplication)
                {
                    if (HttpContext.Current.CurrentHandler != null && app.Response.ContentType == "text/html")
                    {
                        string handler = HttpContext.Current.CurrentHandler.GetType().ToString();
                        if (handler == "Umbraco.Web.Mvc.UmbracoMvcHandler" || handler == "umbraco.UmbracoDefault")
                        {
                            var context = UmbracoContext.Current;
                            
                            if (context.Security.CurrentUser != null && context.PageId != null 
                                && context.Security.CurrentUser.AllowedSections.Contains("content"))
                            {
                                app.Response.Filter = new FrontEndEditFilter(app.Response.Filter);
                            }
                        }
                    }
                }
            }
        }

        public void Dispose()
        {
        }
    }

    class FrontEndEditFilter : FilterBase
    {
        public override void Write(byte[] buffer, int offset, int count)
        {
            byte[] data = new byte[count];
            Buffer.BlockCopy(buffer, offset, data, 0, count);
            string html = System.Text.Encoding.Default.GetString(buffer);

            var context = UmbracoContext.Current;

            var appPluginsPath = SystemDirectories.AppPlugins;

            var macro = new MacroWrapper("[Umbraco]FrontEndToolbar");

            string toolbar = macro.Execute();

            int body = html.IndexOf("</body", StringComparison.InvariantCultureIgnoreCase);

            if (body >= 0 && body <= html.Length)
                html = html.Insert(body, toolbar);

            byte[] outdata = System.Text.Encoding.Default.GetBytes(html);
            _sink.Write(outdata, 0, outdata.GetLength(0));
        }
    }

    class FilterBase : Stream 
    {
        private Stream _sink;

        public FilterBase(Stream sink)
        {
            _sink = sink;
        }

        #region Properties

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override void Flush()
        {
            _sink.Flush();
        }

        public override long Length
        {
            get { return 0; }
        }

        private long _position;
        public override long Position
        {
            get { return _position; }
            set { _position = value; }
        }

        #endregion

        #region Methods

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _sink.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _sink.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _sink.SetLength(value);
        }

        public override void Close()
        {
            _sink.Close();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _sink.Write(buffer, 0, buffer.GetLength(0));
        }


        #endregion

    }


}
