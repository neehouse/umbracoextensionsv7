﻿using HSC.Common.WVU.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Umbraco.Core.Dynamics;
using Umbraco.Core.IO;

namespace HSC.Common.WVU
{
    public static class Calendar
    {
        private static string cacheFilePath = "~/App_Data/TEMP/RemoteContent/";

        #region Remote Data Feed
        private static bool FetchRemoteContent(string url, string cacheFile)
        {
            var cacheFileFullPath = IOHelper.MapPath(cacheFilePath + cacheFile);

            if (!Directory.Exists(IOHelper.MapPath(cacheFilePath)))
                Directory.CreateDirectory(IOHelper.MapPath(cacheFilePath));

            try
            {
                var request = HttpWebRequest.Create(url);

                var response = (HttpWebResponse)(request.GetResponse());
                var reader = new StreamReader(response.GetResponseStream());
                var feedData = reader.ReadToEnd();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    TextWriter tw = new StreamWriter(cacheFileFullPath);
                    tw.Write(feedData);
                    tw.Close();
                    return true;
                }
            }
            catch 
            {
                return false;
            }

            return true;
        }
        public static bool RemoteContentCached(string categories, string cacheFile, DateTime? sDate = null, DateTime? eDate = null, int cacheInSeconds = 0)
        {
            // check for cache
            var cacheFileFullPath = IOHelper.MapPath(cacheFilePath + cacheFile);

            if (File.Exists(cacheFileFullPath))
            {
                if (File.GetLastWriteTime(cacheFileFullPath).AddSeconds(cacheInSeconds) > DateTime.Now)
                {
                    return true;
                }
            }
            // if no cache, go fetch it
            string wvuCalendarUrl = "http://cal.wvu.edu/downloadevents.aspx?export=export&type=N&ctgrys={0}&fType=xml&dType=&xmlbinary=ref&approved=approved";

            var url = string.Format(wvuCalendarUrl, categories);                                  

            var dateRange = string.Empty;
            if (sDate != null) dateRange += string.Format("&sDate={0}", sDate);
            if (eDate != null) dateRange += string.Format("&eDate={0}", eDate);

            return FetchRemoteContent(url + dateRange, cacheFile);
        }
        #endregion

        #region Data Helpers
        public static IEnumerable<CalendarEventModel> AllEvents(string cacheFile)
        {
            var xml = string.Empty;
            //DynamicXml dynamicXml = null;

            var pfs = new PhysicalFileSystem("~/");
            var cacheFileFullPath = IOHelper.MapPath(cacheFilePath + cacheFile);

            if (pfs.FileExists(cacheFileFullPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CalendarModel));
                using (XmlReader reader = XmlReader.Create(cacheFileFullPath))
                {
                    return ((CalendarModel)serializer.Deserialize(reader)).Events;
                }

            }

            return null;



            //using (StreamReader reader = new StreamReader(pfs.OpenFile(cacheFileFullPath), Encoding.UTF8))
            //{
            //    xml = reader.ReadToEnd();
            //}
            //dynamicXml = new DynamicXml(xml);

            //var events = ((IEnumerable<dynamic>)(((dynamic)dynamicXml).EVENT)).OrderBy(x => DateTime.Parse(x.StartDate.InnerText));

            //return events;
        }

        public static bool HasEvents(this IEnumerable<CalendarEventModel> events, DateTime date)
        {
            var beginning = new DateTime(date.Year, date.Month, date.Day);
            var end = beginning.AddDays(1);
            return events.Where(x => DateTime.Parse(x.StartDate.InnerText) < end && DateTime.Parse(x.EndDate.InnerText) >= beginning).Any();
        }

        public static IEnumerable<CalendarEventModel> EventsUpcoming(this IEnumerable<CalendarEventModel> events)
        {
            return events.Where(x => DateTime.Parse(x.StartDate.InnerText) >= DateTime.Now.AddHours(-12) || DateTime.Parse(x.EndDate.InnerText) >= DateTime.Now);
        }

        //public static IEnumerable<dynamic> EventsByDateRange(this IEnumerable<dynamic> events, DateTime start, DateTime end)
        //{
        //    return events.Where(x => DateTime.Parse(x.StartDate.InnerText) >= DateTime.Now || DateTime.Parse(x.EndDate.InnerText) >= DateTime.Now);
        //}

        public static IEnumerable<CalendarEventModel> EventsByYear(this IEnumerable<CalendarEventModel> events, DateTime date)
        {
            return events.Where(x => DateTime.Parse(x.StartDate.InnerText).Year <= date.Year && DateTime.Parse(x.EndDate.InnerText).Year >= date.Year);
        }

        public static IEnumerable<CalendarEventModel> EventsByMonth(this IEnumerable<CalendarEventModel> events, DateTime date)
        {
            var beginning = new DateTime(date.Year, date.Month, 1);
            var end = beginning.AddMonths(1).AddMilliseconds(-1);
            return events.Where(x => DateTime.Parse(x.StartDate.InnerText) <= end && DateTime.Parse(x.EndDate.InnerText) >= beginning);
        }

        public staticIEnumerable<CalendarEventModel> EventsByDate(this IEnumerable<CalendarEventModel> events, DateTime date)
        {
            return events.Where(x => DateTime.Parse(x.StartDate.InnerText) == date && DateTime.Parse(x.EndDate.InnerText) == date);
        }

        public static dynamic GetEvent(this IEnumerable<CalendarEventModel> events, string seriesId, string occurrenceId)
        {
            return events.Where(x => x.ImportSeriesId.InnerText == seriesId && x.ImportOccurrenceId.InnerText == occurrenceId).FirstOrDefault();
        }

        #endregion

        #region Url Helpers
        public static string WvuEventUrl(CalendarEventModel item)
        {
            return string.Format("http://cal.wvu.edu/EventList.aspx?view=EventDetails&eventidn={0}&information_id={1}",
                                 item.ImportSeriesId.InnerText,
                                 item.ImportOccurrenceId.InnerText
                                );
        }

        public static string UmbracoEventUrl(dynamic item, string calendarPageUrl = "")
        {
            return string.Format("{0}?view=EventDetails&eventidn={1}&information_id={2}",
                                 calendarPageUrl,
                                 item.ImportSeriesId.InnerText,
                                 item.ImportOccurrenceId.InnerText
                                );
        }

        public static string UmbracoCalendarUrl(string calendarPageUrl, string view = "Upcoming", string date = "")
        {
            // Upcoming, Calendar, Year, Month, Day
            return string.Format("{0}?view={1}&date={2}",
                                 calendarPageUrl,
                                 view,
                                 date
                                );
        }

        #endregion
    }
}
