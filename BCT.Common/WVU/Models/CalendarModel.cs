﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace HSC.Common.WVU.Models
{
    public class CalendarModel
    {
        [XmlArrayItem("EVENT")]
        public IEnumerable<CalendarEventModel> Events { get; set; }
    }
    public class CalendarEventModel
    {
        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public string Description { get; set; }

        [XmlElement]
        public string ContactName { get; set; }

        [XmlElement]
        public string ContactPhone { get; set; }

        [XmlElement]
        public string ContactEmail { get; set; }

        [XmlElement]
        public string Department { get; set; }

        [XmlElement]
        public string Categorization { get; set; }

        [XmlElement]
        public string PrivateFlag { get; set; }

        [XmlElement]
        public string Highlight { get; set; }

        [XmlArray("Locations")]
        [XmlArrayItem("Location")]
        public IEnumerable<CalendarLocationModel> Location { get; set; }

        [XmlArray("Resources")]
        [XmlArrayItem("Resource")]
        public IEnumerable<CalendarResourceModel> Resources { get; set; }

        [XmlElement]
        public string County { get; set; }

        [XmlElement]
        public string Country { get; set; }

        [XmlElement]
        public string RegistrationEnabled { get; set; }

        [XmlElement]
        public string RegistrationMaxReg { get; set; }

        [XmlElement]
        public string RegistrationDisplaySpaceAvail { get; set; }

        [XmlElement]
        public string Building { get; set; }

        [XmlElement]
        public string Room { get; set; }

        [XmlElement]
        public string RoomDivision { get; set; }

        [XmlElement]
        public string RoomLink { get; set; }

        [XmlElement]
        public string ExternalField1 { get; set; }

        [XmlElement]
        public string ExternalField2 { get; set; }

        [XmlElement]
        public string ExternalField3 { get; set; }

        [XmlElement]
        public string ExternalField4 { get; set; }

        [XmlElement]
        public string AllDayFlag { get; set; }

        [XmlElement]
        public DateTime StartDate { get; set; }

        [XmlElement]
        public string StartTime { get; set; }

        [XmlElement]
        public DateTime EndDate { get; set; }

        [XmlElement]
        public string EndTime { get; set; }

        [XmlElement]
        public string OneTimeSeries { get; set; }

        [XmlElement]
        public string Status { get; set; }

        [XmlElement]
        public string Address1 { get; set; }

        [XmlElement]
        public string Address2 { get; set; }

        [XmlElement]
        public string City { get; set; }

        [XmlElement]
        public string State { get; set; }

        [XmlElement]
        public string Zipcode { get; set; }

        [XmlElement]
        public string Phone { get; set; }

        [XmlElement]
        public string LocationUrl { get; set; }

    }

}