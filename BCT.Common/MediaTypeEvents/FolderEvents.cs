﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using umbraco.cms.presentation.Trees;
using umbraco.BusinessLogic.Actions;
using Umbraco.Web.Trees;
using BCT.Common.DocumentTypeEvents;


namespace BCT.Common.MediaTypeEvents
{
    /// <summary>
    /// Summary description for NewsEvents
    /// </summary>
    public class FolderEvents : ApplicationEventHandler
    {
        const string targetAlias = "Folder";

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            MediaService.Deleting += MediaService_Deleting;
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;
        }

        void MediaService_Deleting(IMediaService sender, Umbraco.Core.Events.DeleteEventArgs<IMedia> e)
        {
            // since there is more than one way to delete the folders, etc, 
            // we have to handle the deleting event.
            foreach(var entity in e.DeletedEntities)
            {
                if (entity.ContentType.Alias == targetAlias)
                {
                    var cs = ApplicationContext.Current.Services.ContentService;
                    var cts = ApplicationContext.Current.Services.ContentTypeService;
                    var websites = cs.GetContentOfContentType(cts.GetContentType("Website").Id);

                    if (websites.Where(x => x.GetValue<int>("umbracoMediaFolderId") == entity.Id).Any())
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            if (e.Menu != null && sender.TreeAlias == "media" && !helper.isAdmin())
            {
                var cs = ApplicationContext.Current.Services.ContentService;
                var ms = ApplicationContext.Current.Services.MediaService;
                var cts = ApplicationContext.Current.Services.ContentTypeService;
                var websites = cs.GetContentOfContentType(cts.GetContentType("Website").Id);
                var content = ms.GetById(int.Parse(e.NodeId));
                var user = UmbracoContext.Current.Security.CurrentUser;

                if (content != null && content.ContentType.Alias == targetAlias)
                {
                    // if a Website references this folder, then remove delete.
                    if (websites.Where(x => x.GetValue<int>("umbracoMediaFolderId") == content.Id).Any())
                    {
                        helper.RemoveMenuAction(e.Menu, "delete");
                    }
                }

                if (e.NodeId == "-1")
                {
                    helper.RemoveMenuAction(e.Menu, "create");
                    helper.RemoveMenuAction(e.Menu, "sort");
                }
            }
        }
    }
}