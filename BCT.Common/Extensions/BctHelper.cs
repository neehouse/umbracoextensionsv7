﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Umbraco.Core;
using Umbraco.Core.Dynamics;
using Umbraco.Core.IO;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace BCT.Common.Extensions
{
    /// <summary>
    /// Summary description for ExtensionMethods
    /// </summary>
    public static class BctHelper
    {
        public static string NullIfEmpty(this string @this)
        {
            return (@this == null || @this.Trim().Length == 0) ? null : @this;
        }

        public static bool HasChildren(this IPublishedContent content)
        {
            return ((IEnumerable<IPublishedContent>)content.Children).Any();
        }


    }
}