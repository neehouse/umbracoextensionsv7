﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using umbraco.presentation.templateControls;

namespace HSC.Common.HttpModules
{
    public class MacroWrapper
    {
        private Macro macro;

        public AttributeCollection Attributes;

        public MacroWrapper(string alias)
        {
            macro = new Macro();
            Attributes = new AttributeCollection(new StateBag());
            macro.Alias = alias;
        }


        public string Execute()
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            foreach(string attribute in Attributes.Keys)
            {
                macro.Attributes.Add(attribute, Attributes[attribute]);
            }
            macro.RenderControl(hw);

            return sw.ToString();

        }
    }
}
