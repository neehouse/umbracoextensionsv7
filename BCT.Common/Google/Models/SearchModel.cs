﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCT.Common.Google.Models
{
    public class SearchModel
    {
        [JsonProperty(PropertyName = "kind")]
        public string Kind { get; set; }

        [JsonProperty(PropertyName = "url")]
        public SearchUrlModel Url { get; set; }

        [JsonProperty(PropertyName = "queries")]
        public IDictionary<string, IEnumerable<SearchRequestModel>> Queries { get; set; }

        [JsonProperty(PropertyName = "promotions")]
        public IEnumerable<SearchPromotionsModel> Promotions { get; set; }

        [JsonProperty(PropertyName = "context")]
        public SearchContextModel Context { get; set; }

        [JsonProperty(PropertyName = "searchInformation")]
        public SearchInformationModel SearchInformation { get; set; }


        [JsonProperty(PropertyName = "items")]
        public IEnumerable<SearchResultModel> Results { get; set; }
    }

    public class SearchUrlModel
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "template")]
        public string Template { get; set; }
    }
    public class SearchPromotionsModel
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "htmlTitle")]
        public string HtmlTitle { get; set; }

        [JsonProperty(PropertyName = "Link")]
        public string Link { get; set; }

        [JsonProperty(PropertyName = "displayLink")]
        public string DisplayLink { get; set; }

        [JsonProperty(PropertyName = "bodyLines")]
        public IEnumerable<SearchBodyLinesModel> BodyLines { get; set; }

        [JsonProperty(PropertyName = "image")]
        public SearchImageCseModel Image { get; set; }
    }

    public class SearchContextModel
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "facets")]
        public IEnumerable<SearchFacetsModel> Facets { get; set; }
    }

    public class SearchInformationModel
    {
        [JsonProperty(PropertyName = "searchTime")]
        public double SearchTime { get; set; }

        [JsonProperty(PropertyName = "formattedSearchTime")]
        public string FormattedSearchTime { get; set; }

        [JsonProperty(PropertyName = "totalResults")]
        public int TotalResults { get; set; }

        [JsonProperty(PropertyName = "formattedTotalResults")]
        public string FormattedTotalResults { get; set; }
    }

    public class SearchSpellingModel
    {
        [JsonProperty(PropertyName = "correctedQuery")]
        public string CorrectedQuery { get; set; }

        [JsonProperty(PropertyName = "htmlCorrectedQuery")]
        public string HtmlCorrectedQuery { get; set; }
    }

    public class SearchResultModel
    {
        [JsonProperty(PropertyName = "kind")]
        public string Kind { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "htmlTitle")]
        public string HtmlTitle { get; set; }

        [JsonProperty(PropertyName = "link")]
        public string Link { get; set; }

        [JsonProperty(PropertyName = "displayLink")]
        public string DisplayLink { get; set; }

        [JsonProperty(PropertyName = "snippet")]
        public string Snippet { get; set; }

        [JsonProperty(PropertyName = "htmlSnippet")]
        public string HtmlSnippet { get; set; }

        [JsonProperty(PropertyName = "cacheId")]
        public string CacheId { get; set; }

        [JsonProperty(PropertyName = "mime")]
        public string Mime { get; set; }

        [JsonProperty(PropertyName = "fileFormat")]
        public string FileFormat { get; set; }

        [JsonProperty(PropertyName = "formattedUrl")]
        public string FormattedUrl { get; set; }

        [JsonProperty(PropertyName = "htmlFormattedUrl")]
        public string HtmlFormattedUrl { get; set; }

        [JsonProperty(PropertyName = "pagemap")]
        public SearchPageMapModel PageMap { get; set; }

        [JsonProperty(PropertyName = "labels")]
        public IEnumerable<SearchLabelsModel> Labels { get; set; }

        [JsonProperty(PropertyName = "image")]
        public SearchImageModel Image { get; set; }
    }

    public class SearchRequestModel
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "totalResults")]
        public int TotalResults { get; set; }

        [JsonProperty(PropertyName = "searchTerms")]
        public string SearchTerms { get; set; }

        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "startIndex")]
        public int StartIndex { get; set; }

        [JsonProperty(PropertyName = "inputEncoding")]
        public string InputEncoding { get; set; }

        [JsonProperty(PropertyName = "outputEncoding")]
        public string OutputEncoding { get; set; }

        [JsonProperty(PropertyName = "save")]
        public string Safe { get; set; }

        [JsonProperty(PropertyName = "cx")]
        public string CustomSearchId { get; set; }
    }
    public class SearchBodyLinesModel
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "htmlTitle")]
        public string HtmlTitle { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "link")]
        public string Link { get; set; }

    }
    public class SearchPageMapModel
    {
        [JsonProperty(PropertyName = "metatags")]
        public IEnumerable<SearchMetaTagsModel> MetaTags { get; set; }

        [JsonProperty(PropertyName = "cse_image")]
        public IEnumerable<SearchImageCseModel> Image { get; set; }

        [JsonProperty(PropertyName = "cse_thumbnail")]
        public IEnumerable<SearchImageCseModel> Thumbnail { get; set; }
    }

    public class SearchMetaTagsModel
    {
        [JsonProperty(PropertyName = "author")]
        public string Author { get; set; }

        [JsonProperty(PropertyName = "creator")]
        public string Creator { get; set; }

        [JsonProperty(PropertyName = "creationdate")]
        public string Created { get; set; }

        [JsonProperty(PropertyName = "moddate")]
        public string Modified { get; set; }

        [JsonProperty(PropertyName = "producer")]
        public string Producer { get; set; }

        [JsonProperty(PropertyName = "viewport")]
        public string Viewport { get; set; }

    }

    public class SearchImageCseModel
    {
        [JsonProperty(PropertyName = "width")]
        public int Width { get; set; }

        [JsonProperty(PropertyName = "height")]
        public int Height { get; set; }

        [JsonProperty(PropertyName = "src")]
        public string Source { get; set; }
    }

    public class SearchFacetsModel
    {
        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "anchor")]
        public string Anchor { get; set; }

        [JsonProperty(PropertyName = "label_with_op")]
        public string OpLabel { get; set; }
    }

    public class SearchLabelsModel
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "displayName")]
        public string DisplayName { get; set; }

        [JsonProperty(PropertyName = "label_with_op")]
        public string OpLabel { get; set; }
    }

    public class SearchImageModel
    {
        [JsonProperty(PropertyName = "contextLink")]
        public string contextLink { get; set; }

        [JsonProperty(PropertyName = "height")]
        public int height { get; set; }

        [JsonProperty(PropertyName = "width")]
        public int width { get; set; }

        [JsonProperty(PropertyName = "byteSize")]
        public int byteSize { get; set; }

        [JsonProperty(PropertyName = "thumbnailLink")]
        public string thumbnailLink { get; set; }

        [JsonProperty(PropertyName = "thumbnailHeight")]
        public int thumbnailHeight { get; set; }

        [JsonProperty(PropertyName = "thumbnailWidth")]
        public int thumbnailWidth { get; set; }
    }
}