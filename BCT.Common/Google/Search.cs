﻿using BCT.Common.Google.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using umbraco.IO;

namespace BCT.Common.Google
{
    public class Search
    {
        public string ApiKey { get; set; }
        public string CustomSearchId { get; set; }
        public int Count { get; set; }

        public Search()
        {
        }

        public Search(string apiKey)
        {
            ApiKey = apiKey;
        }

        public Search(string apiKey, string customSearchId)
        {
            ApiKey = apiKey;
            CustomSearchId = customSearchId;
        }


        public SearchModel GetSearchResults(string query, int page = 1)
        {
            if (ApiKey == null)
                throw new Exception("Google Search API requires the API Key.");

            var customSearch = string.Empty;
            if (CustomSearchId != null)
                customSearch = "&cx=" + CustomSearchId;

            var start = (Count * (page - 1)) + 1;

            var searchApiUrl = "https://www.googleapis.com/customsearch/v1?key={0}{1}&start={3}&count={4}&q={2}";

            var searchUrl = string.Format(searchApiUrl, ApiKey, customSearch, HttpUtility.UrlEncode(query), start, Count);

            var resultJson = FetchRemoteContent(searchUrl);

            return JsonConvert.DeserializeObject<SearchModel>(resultJson);
        }

        private string FetchRemoteContent(string url)
        {

            using (var response = (HttpWebResponse)(HttpWebRequest.Create(url).GetResponse()))
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            return null;
        }
    }
}