﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using umbraco.cms.presentation.Trees;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Security;
using Umbraco.Web.Trees;
using Umbraco.Web.WebApi;

namespace BCT.Common.Trees
{
    public class MemberTree : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;
        }

        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;
            if (e.Menu != null && e.QueryStrings.Get("application") == "member" && e.QueryStrings.Get("id").Length == 32 ) 
            {
                var js = @"
                            var nodeId =UmbClientMgr.mainTree().getActionNode().nodeId;
                            var result = jQuery.post('../Umbraco/BackOffice/BCT/ImpersonateMember/Impersonate?id=' + nodeId).done(function(data) {
                                            if(data=='Success') {
                                                window.open('/');
                                            } else { alert('Impersonate Member failure.\nMessage: ' + data); }
                                         });
                         "
                        .Replace("\n"," ");


                    e.Menu.Items.Insert(0, new Umbraco.Web.Models.Trees.MenuItem("impersonate", "Impersonate Member") { Icon = "shuffle", AdditionalData = {{ "jsAction" , js}} });
            }
        }
    }

    [PluginController("BCT")]
    public class ImpersonateMemberController : UmbracoAuthorizedApiController
    {
        [System.Web.Http.HttpPost]
        public string Impersonate([FromUri] string id)
        {
            var user = Security.CurrentUser;

            if (user.AllowedSections.Contains("member"))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var ms = ApplicationContext.Current.Services.MemberService;

                    var member = ms.GetByKey(new Guid(id));
                    
                    if (member != null)
                    {
                        var provider = Membership.Providers[Constants.Conventions.Member.UmbracoMemberProviderName];
                        var mpu = provider.GetUser(member.Username, true);
                        FormsAuthentication.SetAuthCookie(mpu.UserName, true);

                        return "Success";
                    }
                    else
                    {
                        return "Member Not Found.";
                    }
                }
                else
                {
                    return "Invalid Member Id.";
                }
            }
            else 
            {
                return "Must have access to Members to Impersonate a Member.";
            }
        }
    }
}