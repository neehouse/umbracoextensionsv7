﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using umbraco.cms.presentation.Trees;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;
using Umbraco.Web.WebApi;

namespace BCT.Common.Trees
{
    public class UserTree : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;

            RouteTable.Routes.MapRoute("ImpersonateUser", "umbraco/BACKOFFICE/impersonate-user/{id}", new { controller = "ImpersonateUser", action = "Impersonate" });
        }

        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            var user = UmbracoContext.Current.Security.CurrentUser;   
            if (e.Menu != null && e.QueryStrings.Get("treeType") == "users" && user.UserType.Alias == "admin") 
            {
                var menuUserId = -1;
                int.TryParse(e.QueryStrings.Get("id"), out menuUserId);

                var js = @"
                            var nodeId =UmbClientMgr.mainTree().getActionNode().nodeId;
                            var result = jQuery.post('../umbraco/BACKOFFICE/impersonate-user/' + nodeId).done(function(data) {
                                            if(data=='Success') {
                                                window.location.href = '../umbraco/';
                                            } else { alert('Impersonate User failure.\nMessage: ' + data); }
                                         });
                         "
                        .Replace("\n"," ");


                if (menuUserId != user.Id && menuUserId > 0)
                {
                    e.Menu.Items.Insert(0, new Umbraco.Web.Models.Trees.MenuItem("impersonate", "Impersonate User") { Icon = "shuffle", AdditionalData = {{ "jsAction" , js}} });
                }
            }
        }
    }

    // This is not an API Controller because I used a session variable.  API has no session.
    public class ImpersonateUserController : UmbracoAuthorizedController
    {
        [System.Web.Http.HttpPost]
        public string Impersonate(int id)
        {
            // current user
            var currentUser = UmbracoContext.Current.Security.CurrentUser;

            if (currentUser == null) 
            {
                return "Not currently signed in.";
            }

            if (currentUser.UserType.Alias == "admin")
            {
                if (id > 0)
                {
                    var us = ApplicationContext.Current.Services.UserService;
                    var newUser = us.GetUserById(id);
                    if (newUser != null)
                    {
                        UmbracoContext.Current.HttpContext.Session.Add("Impersonate.User.Id", currentUser.Id);

                        UmbracoContext.Current.Security.ClearCurrentLogin();
                        UmbracoContext.Current.Security.PerformLogin(newUser.Id);

                        return "Success";
                    }
                    else
                    {
                        return "User Not Found.";
                    }
                }
                else
                {
                    return "Invalid User Id.";
                }
            }
            else 
            {
                return "Must be an Administrator to impersonate another user.";
            }
        }
    }
}