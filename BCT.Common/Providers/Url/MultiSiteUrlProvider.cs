﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace BCT.Common.Providers.Url
{
    public class RegisterUrlProvider : ApplicationEventHandler
    {
        protected override void ApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var urlpr = UrlProviderResolver.Current;


            urlpr.InsertTypeBefore(typeof(DefaultUrlProvider),typeof(MultiSiteUrlProvider));
            urlpr.RemoveType(typeof(DefaultUrlProvider));
        }
    }

    public class MultiSiteUrlProvider: DefaultUrlProvider
    {
        public override string GetUrl(Umbraco.Web.UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            var route = umbracoContext.ContentCache.GetRouteById(id);
            // extract domainUri and path
            // route is /<path> or <domainRootId>/<path>
            var pos = route.IndexOf('/');
            var path = pos == 0 ? route : route.Substring(pos);
            var newPath = path;

            var content = umbracoContext.ContentCache.GetById(id);
            if (content != null)
            {
                var rootWebsite = umbracoContext.ContentCache.GetAtRoot().FirstOrDefault();
                if (content.Path.StartsWith(rootWebsite.Path))
                {
                    var rootSitePath = "/" + rootWebsite.UrlName;

                    if (path.StartsWith(rootSitePath))
                    {
                        newPath = path.Substring(rootSitePath.Length);
                        if (string.IsNullOrEmpty(newPath)) newPath = "/";
                    }
                }
                if (content.DocumentTypeAlias == "HomePage")
                {
                    var website = content.Ancestors("Website").FirstOrDefault();
                    // is this the website's referenced homepage?
                    if (website.GetPropertyValue<int>("umbracoInternalRedirectId") == content.Id)
                    {
                        // use the website url instead.
                        return website.Url;
                    }
                }
            }
            var baseUrl = base.GetUrl(umbracoContext, id, current, mode);
            var remove = baseUrl.Length - path.Length - 1;
            if (remove < 0) remove = 0;
            return baseUrl.Remove(remove) + newPath;
        }

    }
}