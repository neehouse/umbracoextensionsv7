﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using umbraco.BasePages;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Trees;

namespace BCT.Common.DocumentTypeEvents
{
    public static class helper
    {
        public static IContent GetNode(IContent parent, string name, string contentTypeAlias, bool hide = false, bool hideChild = false)
        {
            IContent folder = parent.Children().Where(x => x.Name == name && x.ContentType.Alias == contentTypeAlias).FirstOrDefault();
            if (folder == null)
            {
                var cs = ApplicationContext.Current.Services.ContentService;
                folder = cs.CreateContent(name, parent, contentTypeAlias);

                if (hide)
                    folder.SetValue("umbracoNaviHide", true);

                if (hideChild)
                    folder.SetValue("umbracoNaviHideChild", true);

                cs.SaveAndPublishWithStatus(folder);
            }
            return folder;
        }

        public static void CreatMonthFolders(IContent parent, string contentTypeAlias)
        {
            DateTime date = DateTime.Parse("1/1/1900");
            for (var i = 0; i < 12; i++)
            {
                GetNode(parent, date.AddMonths(i).ToString("MMMM"), contentTypeAlias, true);
            }
        }

        public static void RemoveMenuAction(MenuItemCollection menu, string action)
        {
            if (menu != null)
            {
                // If it has create, see if it needs it.
                if (menu.Items.Where(x => x.Alias == action).Any())
                {
                    menu.Items.RemoveAll(x => x.Alias == action);
                }
            }
        }

        public static bool isAdmin()
        {
            return UmbracoContext.Current.Security.CurrentUser.UserType.Alias == "admin";
        }

        public static void RedirectToSelf()
        {
            //if (HttpContext.Current != null)
            //    BasePage.Current.ClientTools.ChangeContentFrameUrl(HttpContext.Current.Request.RawUrl);
       }
    }
}
