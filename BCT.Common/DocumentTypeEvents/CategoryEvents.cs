﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using umbraco.cms.presentation.Trees;
using umbraco.BusinessLogic.Actions;
using Umbraco.Web.Trees;


namespace BCT.Common.DocumentTypeEvents
{
    /// <summary>
    /// Summary description for NewsEvents
    /// </summary>
    public class CategoryEvents : ApplicationEventHandler
    {
        const string targetAlias = "CategoryFolder";

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;
        }

        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            if (e.Menu != null && sender.TreeAlias == "content" && !helper.isAdmin())
            {
                var cs = ApplicationContext.Current.Services.ContentService;
                var content = cs.GetById(int.Parse(e.NodeId));
                var user = UmbracoContext.Current.Security.CurrentUser;

                if (content != null && content.ContentType.Alias == targetAlias)
                {
                    helper.RemoveMenuAction(e.Menu, "delete");
                }
            }
        }
    }
}