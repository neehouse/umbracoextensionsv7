﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace BCT.Common.DocumentTypeEvents
{
    /// <summary>
    /// Summary description for NewsEvents
    /// </summary>
    public  class CalendarEvents : ApplicationEventHandler
    {
        const string targetAlias = "Event";
        const string holderAlias = "EventHolder";
        const string folderAlias = "EventDateFolder";
        const string draftsFolderName = "Drafts";
        const string draftsFolderAlias = "DraftsFolder";
        const string datePropertyAlias = "date";
        const string categoryFolderName = "Categories";
        const string categoryFolderAlias = "CategoryFolder";

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Saved += ContentService_Saved;
            ContentService.Publishing += ContentService_Publishing;
            ContentService.UnPublished += ContentService_UnPublished;
        }

        void ContentService_Saved(IContentService sender, Umbraco.Core.Events.SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                if (entity.ContentType.Alias == targetAlias && !entity.Published)
                {
                    IContent parent = entity.Parent();
                    if (parent.ContentType.Alias != draftsFolderAlias)
                    {
                        IContent holder = entity.Ancestors().Where(x => x.ContentType.Alias == holderAlias).FirstOrDefault();
                        var draftsFolder = helper.GetNode(holder, draftsFolderName, draftsFolderAlias, true);
                        entity.ParentId = draftsFolder.Id;
                        sender.Save(entity, raiseEvents: false);
                    }
                }
                if (entity.ContentType.Alias == holderAlias && entity.IsNewEntity())
                {
                    var categoryFolder = helper.GetNode(entity, categoryFolderName, categoryFolderAlias, true);
                    var draftsFolder = helper.GetNode(entity, draftsFolderName, draftsFolderAlias, true);

                    var rss = helper.GetNode(entity, "RSS", "RSSFeed", true);
                    rss.SetValue("rssDocumentType", targetAlias);
                    rss.SetValue("rssTitleProperty", "title");
                    rss.SetValue("rssDescriptionProperty", "introduction");
                    rss.SetValue("rssDateProperty", datePropertyAlias);
                    sender.Save(rss);
                }

            }
        }

        void ContentService_UnPublished(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            foreach (var entity in e.PublishedEntities)
            {
                if (entity.ContentType.Alias == targetAlias)
                {
                    IContent parent = entity.Parent();
                    if (parent.ContentType.Alias != draftsFolderAlias)
                    {
                        IContent holder = entity.Ancestors().Where(x => x.ContentType.Alias == holderAlias).FirstOrDefault();
                        var draftsFolder = helper.GetNode(holder, draftsFolderName, draftsFolderAlias, true);
                        entity.ParentId = draftsFolder.Id;
                        var cs = ApplicationContext.Current.Services.ContentService;
                        cs.Save(entity, raiseEvents: false);
                    }
                }
            }
        }


        void ContentService_Publishing(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            // When we publish a NewsItem, move it to the corresponding date folder
            foreach (var entity in e.PublishedEntities)
            {
                if (entity.ContentType.Alias == targetAlias)
                {
                    DateTime date = entity.GetValue<DateTime>(datePropertyAlias);
                    IContent holder = entity.Parent().ContentType.Alias == holderAlias ? entity.Parent() : entity.Parent().Ancestors().Where(x => x.ContentType.Alias == holderAlias).FirstOrDefault();
                    IContent year = helper.GetNode(holder, date.ToString("yyyy"), folderAlias, false);
                    helper.CreatMonthFolders(year, folderAlias);
                    IContent month = helper.GetNode(year, date.ToString("MMMM"), folderAlias, false);
                    entity.ParentId = month.Id;
                    // Redirect Page to make the tree load correctly.
                    helper.RedirectToSelf();
                }
            }
        }
    }
}