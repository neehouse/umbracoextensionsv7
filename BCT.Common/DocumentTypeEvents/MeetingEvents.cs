﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace BCT.Common.DocumentTypeEvents
{
    /// <summary>
    /// Summary description for NewsEvents
    /// </summary>
    public class MeetingEvents : ApplicationEventHandler
    {
        const string targetAlias = "Meeting";
        const string holderAlias = "MeetingHolder";
        const string FolderAlias = "MeetingDateFolder";
        const string draftsFolderName = "Drafts";
        const string draftsFolderAlias = "DraftsFolder";
        const string datePropertyAlias = "date";
        const string categoryFolderName = "Categories";
        const string categoryFolderAlias = "CategoryFolder";

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Saved += ContentService_Saved;
            ContentService.Publishing += ContentService_Publishing;
        }

        void ContentService_Saved(IContentService sender, Umbraco.Core.Events.SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                if (entity.ContentType.Alias == targetAlias && entity.IsNewEntity())
                {
                    IContent parent = entity.Parent();
                    if (parent.ContentType.Alias == holderAlias)
                    {
                        var draftsFolder = helper.GetNode(parent, draftsFolderName, draftsFolderAlias, true);
                        entity.ParentId = draftsFolder.Id;
                    }

                    var minutes = sender.CreateContent("Minutes", entity.Id, "MeetingMinutes");
                    minutes.SetValue("umbracoNaviHide", true);
                    sender.Save(minutes);
                }
                if (entity.ContentType.Alias == holderAlias && entity.IsNewEntity())
                {
                    var categoryFolder = helper.GetNode(entity, categoryFolderName, categoryFolderAlias, true);
                    var draftsFolder = helper.GetNode(entity, draftsFolderName, draftsFolderAlias, true);

                    var rss = helper.GetNode(entity, "RSS", "RSSFeed", true);
                    rss.SetValue("rssDocumentType", targetAlias);
                    rss.SetValue("rssTitleProperty", "title");
                    rss.SetValue("rssDescriptionProperty", "content");
                    rss.SetValue("rssDateProperty", datePropertyAlias);
                    sender.Save(rss);
                }
            }
        }

        void ContentService_Publishing(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            foreach (var entity in e.PublishedEntities)
            {
                if (entity.ContentType.Alias == targetAlias)
                {
                    DateTime date = entity.GetValue<DateTime>(datePropertyAlias);
                    IContent holder = entity.Parent().ContentType.Alias == holderAlias ? entity.Parent() : entity.Parent().Ancestors().Where(x => x.ContentType.Alias == holderAlias).FirstOrDefault();
                    IContent year = helper.GetNode(holder, date.ToString("yyyy"), FolderAlias, false);
                    entity.ParentId = year.Id;
                }
            }
        }
    }
}