﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace BCT.Common.DocumentTypeEvents
{
    /// <summary>
    /// Summary description for RelateOnCopyEvents
    /// </summary>
    public class RelateOnCopyEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Saved += ContentService_Saved;
            ContentService.Published += ContentService_Published;
        }

        void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            var rs = ApplicationContext.Current.Services.RelationService;
            var cs = ApplicationContext.Current.Services.ContentService;
            foreach (var entity in e.PublishedEntities)
            {
                if (!HttpContext.Current.Items.Contains(string.Format("SkipRelateOnCopyEvents-{0}", entity.Id.ToString())))
                {
                    var relations = rs.GetByParentOrChildId(entity.Id).Where(x => x.RelationType.Alias == "relateDocumentOnCopy");

                    foreach (var relation in relations)
                    {
                        int relatedId = relation.ParentId == entity.Id ? relation.ChildId : relation.ParentId;
                        var relatedContent = cs.GetById(relatedId);

                        if (relatedContent.HasPublishedVersion())
                        {
                            HttpContext.Current.Items.Add(string.Format("SkipRelateOnCopyEvents-{0}", relatedContent.Id.ToString()), true);
                            cs.SaveAndPublishWithStatus(relatedContent, userId: entity.WriterId, raiseEvents: false);
                        }
                    }
                }
            }
        }

        void ContentService_Saved(IContentService sender, Umbraco.Core.Events.SaveEventArgs<Umbraco.Core.Models.IContent> e)
        {
            var rs = ApplicationContext.Current.Services.RelationService;

            var updatedContent = new List<IContent>();
            int userId = 0;

            foreach (var entity in e.SavedEntities)
            {
                var relations = rs.GetByParentOrChildId(entity.Id).Where(x => x.RelationType.Alias == "relateDocumentOnCopy");

                foreach (var relation in relations)
                {
                    int relatedId = relation.ParentId == entity.Id ? relation.ChildId : relation.ParentId;
                    var relatedContent = sender.GetById(relatedId);
                    // update dirty properties
                    foreach (var property in entity.Properties)
                    {
                        relatedContent.SetValue(property.Alias, property.Value);
                    }
                    userId = relatedContent.WriterId;
                    updatedContent.Add(relatedContent);
                }
            }
            sender.Save(updatedContent, userId: userId, raiseEvents: false);
        }
    }
}