﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Security;
using Umbraco.Web.Trees;

namespace BCT.Common.DocumentTypeEvents
{
    /// <summary>
    /// Summary description for SiteHolderEvents
    /// </summary>
    public class WebsiteEvents : ApplicationEventHandler
    {
        const string targetAlias = "Website";
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Saved += ContentService_Saved;
            ContentService.Published += ContentService_Published;
            ContentService.Deleting += ContentService_Deleting;
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;
        }

        void ContentService_Saved(IContentService sender, Umbraco.Core.Events.SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                if (entity.ContentType.Alias == targetAlias && entity.IsNewEntity())
                {
                    // save the node to get the Id, if not already present
                    if (entity.Id == 0)
                        sender.Save(entity, raiseEvents: false);

                    entity.SetValue("siteTitle", entity.Name);

                    var siteSettings = sender.CreateContent("Site Settings", entity.Id, "SiteRepository", entity.CreatorId);
                    siteSettings.SetValue("umbracoNaviHide", true);
                    sender.SaveAndPublishWithStatus(siteSettings);

                    var homePage = sender.CreateContent("Home", entity.Id, "HomePage", entity.CreatorId);
                    sender.SaveAndPublishWithStatus(homePage);

                    var siteSearch = sender.CreateContent("Search", entity.Id, "SearchPage", entity.CreatorId);
                    siteSearch.SetValue("umbracoNaviHide", true);
                    siteSearch.SetValue("collection", "default_collection");
                    siteSearch.SetValue("itemsPerPage", 10);
                    sender.SaveAndPublishWithStatus(siteSearch);

                    var errorPage = sender.CreateContent("Error", entity.Id, "ErrorPage", entity.CreatorId);
                    errorPage.SetValue("umbracoNaviHide", true);
                    sender.SaveAndPublishWithStatus(errorPage);

                    var ms = ApplicationContext.Current.Services.MediaService;

                    var siteMedia = ms.CreateMedia(entity.Name, -1, "Folder", entity.CreatorId);
                    ms.Save(siteMedia);

                    entity.SetValue("umbracoInternalRedirectId", homePage.Id);
                    entity.SetValue("umbracoMediaFolderId", siteMedia.Id);
                    entity.SetValue("umbracoSearchPageId", siteSearch.Id);
                    entity.SetValue("umbracoErrorPageId", errorPage.Id);

                    sender.Save(entity, raiseEvents: false);
                }
            }
        }

        void ContentService_Deleting(IContentService sender, Umbraco.Core.Events.DeleteEventArgs<IContent> e)
        {
            // only administrators can delete.
            foreach (var entity in e.DeletedEntities)
            {
                if (entity.ContentType.Alias == targetAlias)
                {
                    if (!helper.isAdmin())
                    {
                        e.Cancel = true;
                    }
                }
            }
        }
        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            if (e.Menu != null && sender.TreeAlias == "content" && !helper.isAdmin())
            {
                var cs = ApplicationContext.Current.Services.ContentService;
                var content = cs.GetById(int.Parse(e.NodeId));

                if (content != null && content.ContentType.Alias == targetAlias)
                {
                    helper.RemoveMenuAction(e.Menu, "delete");
                }
            }
        }

        void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            foreach (var entity in e.PublishedEntities)
            {
                if (entity.ContentType.Alias == targetAlias)
                {
                    foreach (var child in entity.Children())
                    {
                        switch (child.ContentType.Alias)
                        {
                            case "SiteRepository":
                            case "HomePage":
                            case "SearchPage":
                            case "ErrorPage":
                                if (!child.Published)
                                {
                                    sender.Publish(child, entity.WriterId);
                                }
                                return;
                            default:
                                return;
                        }
                    }
                }
            }
        }
    }
}