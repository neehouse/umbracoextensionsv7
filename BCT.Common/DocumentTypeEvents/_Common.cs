﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;
using umbraco.cms.presentation.Trees;
using Umbraco.Core;
using Umbraco.Core.Models.EntityBase;
using Umbraco.Core.Services;
using Umbraco.Web;
using umbraco.BusinessLogic.Actions;
using Umbraco.Core.Models;
using Umbraco.Web.Trees;

namespace BCT.Common.DocumentTypeEvents
{
    /// <summary>
    /// Summary description for DefaultProperties
    /// </summary>
    public class Common : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Created += ContentService_Created;
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;
        }

        void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            if (e.Menu != null && sender.TreeAlias == "content")
            {
                var cs = ApplicationContext.Current.Services.ContentService;
                var content = cs.GetById(int.Parse(e.NodeId));

                if (content != null && !content.ContentType.AllowedContentTypes.Any())
                    helper.RemoveMenuAction(e.Menu, "create");
            }
        }

        void ContentService_Created(IContentService sender, Umbraco.Core.Events.NewEventArgs<Umbraco.Core.Models.IContent> e)
        {
            var node = e.Entity;
            var us = ApplicationContext.Current.Services.UserService;

            //if (node.HasProperty("pageTitle"))
            //    node.SetValue("pageTitle", node.Name);

            //if (node.HasProperty("navigationTitle"))
            //    node.SetValue("navigationTitle", node.Name);

            //if (node.HasProperty("title"))
            //    node.SetValue("title", node.Name);

            var date = DateTime.Now;

            if (node.HasProperty("date"))
                node.SetValue("date", date);

            if (node.HasProperty("contact"))
                node.SetValue("contact", us.GetUserById(node.CreatorId).Name);
        }
    }
}