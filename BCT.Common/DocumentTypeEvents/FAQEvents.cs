﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;


namespace BCT.Common.DocumentTypeEvents
{
    /// <summary>
    /// Summary description for NewsEvents
    /// </summary>
    public class FAQEvents : ApplicationEventHandler
    {
        const string targetAlias = "FAQItem";
        const string holderAlias = "FAQHolder";
        const string draftsFolderName = "Drafts";
        const string draftsFolderAlias = "DraftsFolder";
        const string categoryFolderName = "Categories";
        const string categoryFolderAlias = "CategoryFolder";

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Saved += ContentService_Saved;
        }

        void ContentService_Saved(IContentService sender, Umbraco.Core.Events.SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                if (entity.ContentType.Alias == holderAlias && entity.IsNewEntity())
                {
                    var categoryFolder = helper.GetNode(entity, categoryFolderName, categoryFolderAlias, true);
                }
            }
        }
    }
}