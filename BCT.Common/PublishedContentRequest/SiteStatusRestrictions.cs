﻿using nuPickers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace BCT.Common.Routing
{
    /// <summary>
    /// Restricts access to sites that have a status other than Production
    /// </summary>
    public class SiteStatusRestrictions : ApplicationEventHandler
    {
        public enum SiteStatus { Public, Intranet, Development, Localhost, Offline };
        
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PublishedContentRequest.Prepared += PublishedContentRequest_Prepared;
        }

        void PublishedContentRequest_Prepared(object sender, EventArgs e)
        {
            PublishedContentRequest pcr = (PublishedContentRequest)sender;
            HttpContext page = HttpContext.Current;

            if (pcr.PublishedContent != null)
            {
                if (pcr.PublishedContent.GetProperty("siteStatus", true).HasValue)
                {
                    string remoteIP = page.Request.UserHostAddress;
                    bool isIntranetUser = (remoteIP.StartsWith("10.") || remoteIP.StartsWith("172.16.") || remoteIP.StartsWith("157.182."));
                    bool isLocalhostUser = (remoteIP == "127.0.0.1" || remoteIP == "::1" || page.Request.Url.Authority.Contains("localhost"));
                    bool isUmbracoUser = (UmbracoContext.Current.Security.CurrentUser != null);
                    bool isMember = false;  // TODO Fix to be dynamic

                    switch (pcr.PublishedContent.GetPropertyValue<Picker>("siteStatus", true).AsEnums<SiteStatus>().First())
                    {
                        case SiteStatus.Offline:
                            ClearWithMessage(503, "This site is offline at this time.");
                            break;
                        case SiteStatus.Development:
                            // serve the page only to back-office users that are signed in.
                            if (!(isUmbracoUser || isLocalhostUser || isLocalhostUser))
                            {
                                ClearWithMessage(401, "This site is restricted to developers at this time.");
                            }
                            break;
                        case SiteStatus.Intranet:
                            if (!(isIntranetUser || isMember || isLocalhostUser || isUmbracoUser))
                            {
                                ClearWithMessage(401, "This site is restricted to intranet users at this time.");
                                // redirect to membership login if site has login page?
                            }
                            break;
                        case SiteStatus.Localhost:
                            if (!(isLocalhostUser || isUmbracoUser))
                            {
                                ClearWithMessage(401 ,"This site is restricted to localhost development at this time.");
                            }
                            break;
                        case SiteStatus.Public:
                            // serve the page.
                            break;
                    }
                }
            }
        }

        private void ClearWithMessage(int statusCode, string message)
        {
            HttpContext page = HttpContext.Current;
            page.Response.Clear();
            page.Response.StatusCode = statusCode;
            page.Response.Write(message);
            page.Response.End();
        }
    }
}