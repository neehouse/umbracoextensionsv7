﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Web.Routing;
using Umbraco.Web;

namespace BCT.UmbracoExtensions.Common.Routing
{
    /// <summary>
    /// Summary description for HomePageTemplateSwitcher
    /// </summary>
    public class HttpsRedirect : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PublishedContentRequest.Prepared += PublishedContentRequest_Prepared;
        }

        void PublishedContentRequest_Prepared(object sender, EventArgs e)
        {
            PublishedContentRequest pcr = (PublishedContentRequest)sender;
            HttpContext page = HttpContext.Current;

            if (pcr.PublishedContent != null && page.Request.Url.Host != "localhost" && page.Request.Url.Host.StartsWith("dev-"))
            {
                bool useHttps = false;
                if (pcr.PublishedContent.GetProperty("umbracoRequireSSL").HasValue && pcr.PublishedContent.GetPropertyValue("umbracoRequireSSL").ToString() == "1")
                    useHttps = true;

                if (useHttps && !page.Request.IsSecureConnection)
                {
                    page.Response.Redirect(page.Request.Url.ToString().Replace("http:", "https:", StringComparison.InvariantCultureIgnoreCase), true);
                }
                else if (!useHttps && page.Request.IsSecureConnection)
                {
                    page.Response.Redirect(page.Request.Url.ToString().Replace("https:", "http:", StringComparison.InvariantCultureIgnoreCase), true);
                }
            }
        }
    }
}