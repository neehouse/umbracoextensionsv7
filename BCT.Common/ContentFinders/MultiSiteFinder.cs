﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace BCT.Common.ContentFinders
{
    public class MultiSiteEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var clcfr = Umbraco.Web.Routing.ContentFinderResolver.Current;

            clcfr.InsertTypeBefore(typeof(ContentFinderByNiceUrl), typeof(MultiSiteContentFinder));
        }
    }
    public class MultiSiteContentFinder : ContentFinderByNiceUrl
    {
        public override bool TryFindContent(PublishedContentRequest docRequest)
        {
            // don't override the default content finder.
            if (base.TryFindContent(docRequest)) { return false; }

            // try finding the page with the added root website path.
            IPublishedContent node = null;
            string path = docRequest.Uri.GetAbsolutePathDecoded();

            var rootWebsite = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault();

            if (docRequest.HasDomain)
                path = DomainHelper.PathRelativeToDomain(docRequest.DomainUri, path);

            path = string.Format("/{0}{1}", rootWebsite.UrlName, path);

            var route = docRequest.HasDomain ? (docRequest.Domain.RootNodeId.ToString() + path) : path;
            node = FindContent(docRequest, route);

            return node != null;
        }
    }
}