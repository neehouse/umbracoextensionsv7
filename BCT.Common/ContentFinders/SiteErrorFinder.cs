﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco.interfaces;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core.Xml;
using Umbraco.Web;
using Umbraco.Web.PublishedCache;
using Umbraco.Web.Routing;

namespace BCT.Common.ContentFinders
{
    public class SiteErrorEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var clcfr = Umbraco.Web.Routing.ContentFinderResolver.Current;

            clcfr.AddType<SiteErrorContentFinder>();
        }
    }

    public class SiteErrorContentFinder : IContentFinder
    {

        public bool TryFindContent(PublishedContentRequest docRequest)
        {
            string route;
            if (docRequest.HasDomain)
                route = docRequest.Domain.RootNodeId.ToString() + DomainHelper.PathRelativeToDomain(docRequest.DomainUri, docRequest.Uri.GetAbsolutePathDecoded());
            else
                route = docRequest.Uri.GetAbsolutePathDecoded();

            // we already tried the full path, so now try parent path
            var node = FindParentContent(docRequest, route);
            if (node != null)
            {
                docRequest.SetIs404();
                return true;
            }
            return false;
        }

        protected IPublishedContent FindParentContent(PublishedContentRequest docreq, string route)
        {
            // we already tried the current requested path.  trim last segment, try it to help find site.
            if (route.LastIndexOf("/") > -1)
                route = route.Substring(0, route.LastIndexOf("/"));

            IPublishedContent errorPage = null;

            // if at root and not found anything
            if (route.Trim("/") == string.Empty)
            {
                var errorPageId = docreq.RoutingContext.UmbracoContext.ContentCache.GetAtRoot().First().GetPropertyValue<int>("umbracoErrorPageId");

                // try domain lookup?
                if (docreq.HasDomain && docreq.Domain.RootNodeId > 0)
                {
                    errorPageId = docreq.RoutingContext.UmbracoContext.ContentCache.GetById(docreq.Domain.RootNodeId).GetPropertyValue<int>("umbracoErrorPageId");
                }

                if (errorPageId > 0)
                {
                    errorPage = docreq.RoutingContext.UmbracoContext.ContentCache.GetById(errorPageId);
                    docreq.PublishedContent = errorPage;
                }
                return null;
            }


            LogHelper.Debug<ContentFinderByNiceUrl>("Test route \"{0}\"", () => route);

            var node = docreq.RoutingContext.UmbracoContext.ContentCache.GetByRoute(route);
            if (node != null)
            {
                var errorPageId = node.AncestorsOrSelf("Website").LastOrDefault().GetPropertyValue<int>("errorPage");
                // if this site does not have an error page, try again.
                if (errorPageId > 0)
                {
                    errorPage = docreq.RoutingContext.UmbracoContext.ContentCache.GetById(errorPageId);
                    docreq.PublishedContent = errorPage;
                    LogHelper.Debug<ContentFinderByNiceUrl>("Got content, id={0}", () => node.Id);
                }
                else
                {
                    errorPage = FindParentContent(docreq, route);
                }
            }
            else
            {
                errorPage = FindParentContent(docreq, route);
            }

            return errorPage;
        }
    }
}
